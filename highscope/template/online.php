<?php 
   /**
   Template Name: Online
   **/
   ?>
<?php get_header(); ?>


<!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>PROFESSIONAL LEARNING</h2>
        </div>

        <div class="row content">
          <div class="col-lg-12">
            Online courses are designed to fit your schedule and your budget. Covering a wide variety of topics, learning takes place within an interactive community of teachers, caregivers, and trainers.



          </div>
          <div class="col-lg-12">
          <?php
            $allowed = array(395,394,393,392,391,390,389,388,387,386);
            $all_categories = get_categories(array('taxonomy'=>'product_cat', 'include' => $allowed));
            foreach ($all_categories as $cat) { ?>
               <div class="col-md-3"> 
                <?php 
                $category_id = $cat->term_id;  
                $category_thumbnail = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                $image = wp_get_attachment_url($category_thumbnail);
                echo '<img src="'.$image.'" class="product-category">';     
                echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

                ?>
               </div>         
            <?php }
            ?>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

<?php wp_footer(); ?>