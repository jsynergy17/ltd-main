<?php 
   /**
   Template Name: Preschool
   **/
   ?>
<?php get_header(); ?>


<!-- ======= About Us Section ======= -->
    <section id="about" class="about shop-subpage">
      <div class="container" data-aos="fade-up">
        <div id="Breadcrumb" class="row-fluid">
            <ul class="breadcrumb">
                <li class="">
                    <a title="Home" href="/">Home</a><span class="divider">&gt;</span>
                </li>
                <li class="active">Preschool</li>
            </ul>
          </div>
        <div class="section-title">
          <h1>PRESCHOOL</h1>
        </div>

        <div class="row content">
          <div class="col-lg-12">
            <p id="categoryDescription"><span  class="cat-sub-title">Implementing HighScope into your preschool program is now easier than ever.</span>Research based and child focused, the HighScope Curriculum uses a carefully designed process&nbsp;— called "active participatory learning"&nbsp;— to achieve powerful, positive outcomes. As teachers, parents, and educational researchers have discovered, the HighScope Preschool Curriculum not only helps young children excel in language and cognitive learning but also promotes independence, curiosity, decision-making, cooperation, persistence, creativity, and problem solving&nbsp;— the fundamental skills that help determine success in adult life.</p>.

          </div>
          <div class="col-lg-12 sub-cat-row">
          <?php
            $allowed = array(273,400,379,376,380,377,378,401,293,381);
            $all_categories = get_categories(array('taxonomy'=>'product_cat', 'include' => $allowed));
            foreach ($all_categories as $cat) { ?>
               <div class="col-md-3 sub-cat-box"> 
                <?php 
                $category_id = $cat->term_id;  
                $category_thumbnail = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                $image = wp_get_attachment_url($category_thumbnail);
                echo '<img src="'.$image.'" class="product-category">';     
                echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

                ?>
               </div>         
            <?php }
            ?>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

<?php get_footer(); ?>