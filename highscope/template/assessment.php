<?php 
   /**
   Template Name: Assessment
   **/
   ?>
<?php get_header(); ?>


<!-- ======= About Us Section ======= -->
    <section id="about" class="about shop-subpage">
      <div class="container" data-aos="fade-up">
        <div id="Breadcrumb" class="row-fluid">
            <ul class="breadcrumb">
                <li class=""><a title="Home" href="/">Home</a><span class="divider">&gt;</span></li>
                <li class="active">Assessment</li>
            </ul>
        </div>
        <div class="section-title">
          <h1>ASSESSMENT</h1>
        </div>

        <div class="row content">
          <div class="col-lg-12">
            <span class="cat-sub-title">HighScope has two comprehensive assessment tools:</span>
            <ul class="shopsub-list">    
                <li>
                    <strong>Child Observation Record (COR)</strong>&nbsp;measures the developmental levels of children, from infancy through kindergarten, and assesses their learning in eight content areas.
                </li>    
                <li><strong>Program Quality Assessment (PQA)</strong>&nbsp;evaluates whether teachers and agencies implement effective program practices.
                </li>
            </ul>
            <p>Because both instruments reflect best practices in the classroom and basic child development principles and research, they are suitable for use in all developmentally based programs, not just those using the HighScope Curriculum.</p>
          </div>
          <div class="col-lg-12 sub-cat-row">
          <?php
            $allowed = array(382,383);
            $all_categories = get_categories(array('taxonomy'=>'product_cat', 'include' => $allowed));
            foreach ($all_categories as $cat) { ?>
               <div class="col-md-3 sub-cat-box"> 
                <?php 
                $category_id = $cat->term_id;  
                $category_thumbnail = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                $image = wp_get_attachment_url($category_thumbnail);
                echo '<img src="'.$image.'" class="product-category">';     
                echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

                ?>
               </div>         
            <?php }
            ?>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

<?php get_footer(); ?>