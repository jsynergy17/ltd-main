<?php 
   /**
   Template Name: Professional Learning
   **/
   ?>
<?php get_header(); ?>


<!-- ======= About Us Section ======= -->
    <section id="about" class="about shop-subpage">
      <div class="container" data-aos="fade-up">
        <div id="Breadcrumb" class="row-fluid">
            <ul class="breadcrumb">
                <li class=""><a title="Home" href="/">Home</a><span class="divider">&gt;</span></li>
                <li class="active">PROFESSIONAL LEARNING</li>
            </ul>
        </div>
        <div class="section-title">
          <h1>PROFESSIONAL LEARNING</h1>
        </div>

        <div class="row content">
          <div class="col-lg-12">
            <p id="categoryDescription">Learning doesn't end when we start teaching others. At HighScope, we believe that professional development is an ongoing part of an educator's journey. Inspiring capable and informed caregivers, teachers and trainers has been our mission for decades...<br><br>There are many paths toward professional growth with HighScope&nbsp;— whether you work with infants, toddlers, preschoolers, or even adults. HighScope offers dozens of courses on a wide array of topics, from one-day to multi-week training, in national venues and local communities. You can even earn credit toward HighScope certification at home through our interactive online courses.<br><br>Questions? Please contact us by phone - 800.587.5639; or email -&nbsp;<a href="mailto:ksherman@highscope.org?subject=Professional Development">ksherman@highscope.org</a>.<br>&nbsp;</p>


          </div>
          <div class="col-lg-12 sub-cat-row">
          <?php
            $allowed = array(384,396,397,398);
            $all_categories = get_categories(array('taxonomy'=>'product_cat', 'include' => $allowed));
            foreach ($all_categories as $cat) { ?>
               <div class="col-md-3 sub-cat-box"> 
                <?php 
                $category_id = $cat->term_id;  
                $category_thumbnail = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                $image = wp_get_attachment_url($category_thumbnail);
                echo '<img src="'.$image.'" class="product-category">';     
                echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

                ?>
               </div>         
            <?php }
            ?>
            <div class="col-md-3 sub-cat-box"> 
                <?php 
                echo '<img src="'.get_stylesheet_directory_uri().'/template/Online Training General.jpg" class="product-category">';     
                echo '<br /><a href="/online/">Online</a>';

                ?>
               </div>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

<?php get_footer(); ?>