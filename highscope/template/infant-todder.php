<?php 
   /**
   Template Name: Infant Todder
   **/
   ?>
<?php get_header(); ?>


<!-- ======= About Us Section ======= -->
    <section id="about" class="about shop-subpage">
      <div class="container" data-aos="fade-up">
        <div id="Breadcrumb" class="row-fluid">
            <ul class="breadcrumb">
                <li class=""><a title="Home" href="/">Home</a><span class="divider">&gt;</span></li>
                <li class="active">Infant-Toddler</li>
            </ul>
        </div>
        <div class="section-title">
          <h1>INFANT-TODDLER</h1>
        </div>

        <div class="row content">
          <div class="col-lg-12">
           <p id="categoryDescription"><span class="cat-sub-title">Research-Based and Child-Focused Curriculum</span>
               The HighScope Infant-Toddler Curriculum uses a carefully designed process of learning through discovery, called active participatory learning. During active learning, infants and toddlers learn about the world around them by exploring and playing. Learning and development are anchored by long-term, trusting relationships with caregivers, who are always close at hand to support children, and consistent yet flexible routines.</p>
           <p>Components include:</p>
           <ul class="shopsub-list">    <li>Proven teaching practices to support children's growth and learning</li>    <li>Curriculum content for infants and toddlers (ages 0–3)</li>    <li>Comprehensive training to help caregivers implement the program effectively</li>    <li>Assessment tools to evaluate and plan for children's learning and to measure program quality</li></ul>
          </div>
          <div class="col-lg-12 sub-cat-row">
          <?php
            $allowed = array(374,375,376,377,378, 293);
            $all_categories = get_categories(array('taxonomy'=>'product_cat', 'include' => $allowed));
            foreach ($all_categories as $cat) { ?>
               <div class="col-md-3 sub-cat-box"> 
                <?php 
                $category_id = $cat->term_id;  
                $category_thumbnail = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                $image = wp_get_attachment_url($category_thumbnail);
                echo '<img src="'.$image.'" class="product-category">';     
                echo '<span class="product-link"><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

                ?>
               </div>         
            <?php }
            ?>
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

<?php get_footer(); ?>