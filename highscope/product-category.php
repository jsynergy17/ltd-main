<?php /* Template Name: Product Category */ ?>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class('product-page page-common'); ?>>

<div id="page-container">
    <div id="top-header">
		<div class="container clearfix">
			<div id="et-secondary-menu">
				<ul class="et-social-icons">
					<?php $social_media = get_field('social_media');
					if($social_media){ $i=0;?>
		            <?php foreach($social_media as $media){ $i++;?>
					<li class="et-social-icon et-social-<?php echo strtolower($media['name']); ?>">
						<a href="<?php echo $media['url']; ?>" class="icon" target="_blank">
							<span><?php echo $media['name']; ?></span>
						</a>
					</li>
					<?php } } ?>
				</ul>
				<ul id="et-secondary-nav" class="menu">
					<?php $top_bar_menu = get_field('top_bar_menu');
					if($top_bar_menu){ $i=0;?>
		            <?php foreach($top_bar_menu as $menu){ $i++;?>
					<li class="<?php echo ($i == count($top_bar_menu))?'button':''; ?> no-mobile menu-item menu-item-type-post_type menu-item-object-page menu-item-266579">
						<a href="<?php echo $menu['menu_url']; ?>" class="da11y-submenu" aria-expanded="false">
							<?php echo $menu['menu_name']; ?>
						</a>
					
					<?php
						if($menu['sub_menu']){ $j=0; ?>
							<ul class="sub-menu">
								<?php foreach($menu['sub_menu'] as $submenu){ $j++;?>
								<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-46710">
									<a href="<?php echo $submenu['inner_menu_url']; ?>" class="da11y-submenu">
									<?php echo $submenu['inner_menu_name']; ?></a>
								</li>
							<?php } ?>
							</ul>
						<?php } ?>
					</li> <?php } } ?>
				</ul>
			</div> <!-- #et-secondary-menu -->
		</div> <!-- .container -->
	</div>
    <header id="main-header" data-height-onload="64" data-height-loaded="true" data-fixed-height-onload="64" style="top: 41.5px;">
			<div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="/">
						<img alt="HighScope" id="logo" data-height-percentage="69" data-src="/wp-content/uploads/2018/01/highscope-logo.png" class=" lazyloaded" src="/wp-content/uploads/2018/01/highscope-logo.png" data-actual-width="456" data-actual-height="67"><noscript><img src="/wp-content/uploads/2018/01/highscope-logo.png" alt="HighScope" id="logo" data-height-percentage="69" /></noscript>
					</a>
				</div>
				<div id="et-top-navigation">
					<?php $menus = wp_get_nav_menu_items("Store Catalog" );?>
					<nav id="top-menu-nav">
						<ul id="top-menu" class="nav">
                            <?php foreach($menus as $value2){ ?>
                                <li class="<?php echo str_replace(' ', '-', $value2->title); ?> <?php echo $value2->classes[0]; ?> mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo $value2->url; ?>" class="da11y-submenu" aria-expanded="false"><?php echo $value2->title; ?></a>
                                </li>
                            <?php } ?>
                        </ul>

                        <a href="/cart/" class="et-cart-icon">
                        <span class="iten-count">
                        	<?php echo WC()->cart->get_cart_contents_count(); ?>
                        </span>
                        </a>
                    </nav>    
						<div class="users_dropdown">
							<button class="dropbtn"></button>
							<div class="users_dropdown-content">
								<?php if(is_user_logged_in()){ ?>
								<a href="/my-account/">My Account</a>
								<?php }else{?>
								<a href="/my-account/">Sign In</a>
								<?php } ?>	
							</div>
						</div>
                    
                        <?php if ( $et_slide_header || is_customize_preview() ) : ?>
                            <span class="mobile_menu_bar et_pb_header_toggle et_toggle_<?php echo esc_attr( et_get_option( 'header_style', 'left' ) ); ?>_menu"></span>
                        <?php endif; ?>

                        <?php

                        /**
                         * Fires at the end of the 'et-top-navigation' element, just before its closing tag.
                         *
                         * @since 1.0
                         */
                        do_action( 'et_header_top' );

                        ?>
				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
            <div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);

						/**
						 * Fires inside the search form element, just before its closing tag.
						 *
						 * @since ??
						 */
						do_action( 'et_search_form_fields' );
					?>
					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header>
	<?php $cate_array = get_breadcrums_category($_GET['c']); ?>	
	<div class="product-banner">
		<div class="container">
			<div class="row">
                <div class="col-12">
                    <nav class="woocommerce-breadcrumb">
                            <a href="/store/">Shop</a>
                            <?php
                            if(!empty($cate_array['first'])){ ?>
                                <span class="addSpace">|</span><a href="/store-category/?c=<?php echo $cate_array['first']; ?>"><?php echo get_the_category_by_ID($cate_array['first']); ?></a>
                            <?php } if(!empty($cate_array['second'])){ ?>
                                <span class="addSpace">|</span><a href="/store-category/?c=<?php echo $cate_array['second']; ?>"><?php echo get_the_category_by_ID($cate_array['second']); ?></a>
                            <?php } if(!empty($cate_array['childs'])){ ?>
                                <span class="addSpace">|</span><a href="/store-category/?c=<?php echo $cate_array['childs']; ?>"><?php echo get_the_category_by_ID($cate_array['childs']); ?></a>
                            <?php } ?>	
                    </nav>
                </div>
                <div class="col-8">

                    <h1>
                        <?php
                        $names = '';
                        if(!empty($cate_array['first'])){ 
                            $idx = $cate_array['first'];
                            $names =  get_the_category_by_ID($cate_array['first']); 
                        }else if(!empty($cate_array['second'])){ 
                            $idx = $cate_array['second'];
                            $names =  get_the_category_by_ID($cate_array['second']);
                        }else if(!empty($cate_array['childs'])){ 
                            $idx = $cate_array['childs'];
                            $names =  get_the_category_by_ID($cate_array['childs']);
                        } ?>
                        <a href="/store-category/?c=<?php echo $idx; ?>"><?php echo $names; ?></a>	
                    </h1>
                </div>
                <div class="col-4">
		    	<div class="ysm-search-widget ysm-search-widget-1 ysm-active">
					<?php echo do_shortcode('[smart_search id="1"]'); ?>
				</div>
		    </div>
	        </div>
	    </div>
	</div>
    <div id="et-main-area">
        <div id="main-content" tabindex="-1" role="main">
			<div class="container">
				<div id="content-area" class="clearfix">
				<?php 
					$per_page = 24;
					if(!empty($_GET['per_page'])){
						$per_page = $_GET['per_page'];
					}
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					
					$args1['orderby'] = 'date';
					$args1['order'] = 'desc';

					if(!empty($_GET['order'])){
						if($_GET['order'] == 'rating'){
							$args1['orderby'] = 'meta_value_num';
							$args1['meta_key'] = '_wc_average_rating';
							$args1['order'] = 'desc';
						}else if($_GET['order'] == 'rating'){
							$args1['orderby'] = 'date';
							$args1['order'] = 'desc';
						}else if($_GET['order'] == 'price'){
							$args1['orderby'] = 'meta_value_num';
							$args1['meta_key'] = '_price';
							$args1['order'] = 'asc';
						}else if($_GET['order'] == 'price-desc'){
							$args1['orderby'] = 'meta_value_num';
							$args1['meta_key'] = '_price';
							$args1['order'] = 'desc';
						}else if($_GET['order'] == 'title'){
							$args1['orderby'] = 'title';
							$args1['order'] = 'asc';
						}else if($_GET['order'] == 'code'){
							$args1['orderby'] = 'meta_value';
							$args1['meta_key'] = '_sku';
							$args1['order'] = 'asc';
						}	
					}	

					$args = array(
						'post_type' => 'product',
						'post_status' => 'publish',
						'ignore_sticky_posts' => 1,
						'posts_per_page' => ($_GET['per_page'])?$_GET['per_page']:24,
						'paged' => $paged,
						'tax_query' => array(
							array(	
							'taxonomy'  => 'product_cat',
							'field'     => 'term_id',
							'terms'     => $_GET['c'],
							'operator'  => 'IN',
						)
						)
					);
					$args = array_merge($args,$args1);
					$loop = new WP_Query( $args );
				?>
					<div id="left-area">
                        <div class="ListingHeader"><p class="woocommerce-result-count">
							Showing <?php echo (($paged-1)*($per_page) == 0)?1:($paged-1)*($per_page); ?>–<?php echo ($paged*$per_page < $loop->found_posts)?$paged*$per_page:$loop->found_posts; ?> of <?php echo $loop->found_posts; ?> Result</p>
    <div class="result_per_page">
        <select name="per_page_data" class="per_page_data">
			<option value="24" <?php echo ($_GET['per_page'] == 24)?'selected':''; ?> >Results Per Page: 24</option>
            <option value="48" <?php echo ($_GET['per_page'] == 48)?'selected':''; ?> >Results Per Page: 48</option>
            <option value="72" <?php echo ($_GET['per_page'] == 72)?'selected':''; ?> >Results Per Page: 72</option>
        </select>
    </div>
<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby1" aria-label="Shop order">
                    <option value="">Sort By</option>
					<option <?php echo ($_GET['order'] == 'rating')?'selected':''; ?> value="rating">Average Rating</option>
					<option <?php echo ($_GET['order'] == 'date')?'selected':''; ?> value="date">Latest</option>
					<option <?php echo ($_GET['order'] == 'price')?'selected':''; ?> value="price">Price Lowest</option>
					<option <?php echo ($_GET['order'] == 'price-desc')?'selected':''; ?> value="price-desc">Price Highest</option>
					<option <?php echo ($_GET['order'] == 'code')?'selected':''; ?> value="code">Code</option>
					<option <?php echo ($_GET['order'] == 'title')?'selected':''; ?> value="title">Title</option>
			</select>
	<input type="hidden" name="paged" value="1">
	</form>
    <nav class="gridlist-toggle"><a href="#" id="grid" title="Grid view" class="active"><span class="dashicons dashicons-grid-view"></span> </a><a href="#" id="list" title="List view"><span class="dashicons dashicons-exerpt-view"></span> </a></nav></div>
	<ul class="products columns-4 grid">
		<?php while ( $loop->have_posts() ) : $loop->the_post();
			global $product;
		?>
    	<li class="pmpro-has-access product type-product post-369478 status-publish first onbackorder product_cat-adult-child-interactions-preschool-curriculum product_cat-social-emotional-preschool-curriculum has-post-thumbnail taxable shipping-taxable purchasable product-type-simple">
			<a href="<?php echo get_the_permalink() ?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
				<span class="et_shop_image img-box">
					<img class="" src="<?php echo (get_the_post_thumbnail_url())?get_the_post_thumbnail_url():'/wp-content/uploads/woocommerce-placeholder.jpg'; ?>">
					<span class="et_overlay"></span>
				</span>
				<h2 class="woocommerce-loop-product__title"><?php echo 
				substr_replace(get_the_title(), "", 45); ?></h2>
			<h6 class="woocommerce-loop-product__discription"><?php 
                $content = strip_tags(get_the_content());
                $content = apply_filters('the_content', $content);
                echo substr_replace($content, "...", 150); ?></h6>
			<div class="product-code">Code: <?php echo (get_post_meta( get_the_ID(), '_sku', true ))?get_post_meta( get_the_ID(), '_sku', true ):'NA'; ?></div>
			<span class="price">
				<span class="woocommerce-Price-amount amount">
					<bdi>
					<?php 
					global $post;
					$product = new WC_Product($post->ID); 
					echo  strip_tags(wc_price($product->get_price_including_tax(1,$product->get_price())));
					?>
                        <span class="p-quantity">/ Each</span>
						<span class="price-tag">Retail price</span>
					</bdi>
				</span>
			</span>
			</a>
			<div class="gridlist-buttonwrap"></div>
		</li>
		<?php endwhile; ?>
	</ul>
    <nav class="woocommerce-pagination">                   
	   <div class="page-numbers">
		<?php 
			echo paginate_links( array(
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'total'        => $loop->max_num_pages,
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'format'       => '?page=%#%',
			'show_all'     => false,
			'type'         => 'plain',
			'end_size'     => 2,
			'mid_size'     => 1,
			'prev_next'    => true,
			'prev_text'    => sprintf( '<i></i> %1$s', __( '<', 'text-domain' ) ),
			'next_text'    => sprintf( '%1$s <i></i>', __( '>', 'text-domain' ) ),
			'add_args'     => false,
			'add_fragment' => '',
		) );
        
		?>
		</div>
    </nav>
	<?php 
	wp_reset_query();
	?> 
</div> 
<?php
	$args = array(
         'taxonomy'     => 'product_cat',
         'orderby'      => 'name',
         'order'        => 'ASC',
         'show_count'   => 0, 
         'pad_counts'   => 0,
         'hierarchical' => 1,
         'title_li'     => '',
         'parent' => 0
  );
 $all_categories = get_categories( $args );

?>
<div id="sidebar">
	<div id="woocommerce_product_categories-5" class="et_pb_widget woocommerce widget_product_categories">
		<h4 class="widgettitle">Categories</h4>
		<ul class="product-categories">
			<?php foreach($all_categories as $value) { ?>
			<li class="cat-item cat-parent <?php echo ($value->term_id == $_GET['c'])?'active':'' ;?>">
				<a href="/store-category/?c=<?php echo $value->term_id; ?>"><?php echo ($value->name == 'ZLast Chance')?'Last Chance':$value->name; ?></a>
				<?php 
					$sub_categ = get_categories(array(
						'taxonomy'     => 'product_cat',
						'orderby'      => 'name',
						'show_count'   => 0, 
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'parent' => $value->term_id
					));
					if(!empty($sub_categ)){ ?>
						<ul class="children">
							<?php foreach($sub_categ as $value2) { ?>
								<li class="cat-item cat-parent  <?php echo ($value2->term_id == $_GET['c'])?'blue-txt':'' ;?>" >
								<a href="/store-category/?c=<?php echo $value2->term_id; ?>"><?php echo $value2->name; ?></a></li>
								<?php
									$sub_cat = get_categories(array(
									'taxonomy'     => 'product_cat',
									'orderby'      => 'name',
									'show_count'   => 0, 
									'pad_counts'   => 0,
									'hierarchical' => 1,
									'title_li'     => '',
									'parent' => $value2->term_id
									));
									if(!empty($sub_cat)){ ?>
										<?php foreach($sub_cat as $value3) { ?> 
										<li class="cat-item cat-parent <?php echo ($value3->term_id == $_GET['c'])?'blue-txt':'' ;?>" >
										<a href="/store-category/?c=<?php echo $value3->term_id; ?>"><?php echo $value3->name; ?></a></li>

									<?php }  } } ?>	
								</ul>	
							<?php } ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div> <!-- #content-area -->
</div> <!-- .container -->
		</div>
        <footer id="main-footer">		
            <div class="container">
                <div id="footer-widgets" class="clearfix">
                    <div class="footer-widget"><div id="custom_html-2" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Join Our Mailing List</h4><div class="textwidget custom-html-widget"><p>
                Be the first to learn about new strategies, products, and events.
            </p>
            <p>
                <a class="btn" href="https://signup.e2ma.net/signup/29012/17220/" onclick="window.open('https://signup.e2ma.net/signup/29012/17220/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up</a>
            </p>
            </div></div>
<div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Stay Connected</h4><div class="textwidget custom-html-widget"><ul class="social">
                                    <li><a href="https://www.facebook.com/HighScopeUS"> 
                                        <img src="/wp-content/themes/highscope/assets/img/facebook.svg"><span>Facebook</span></a></li>
                                    <li><a href="https://twitter.com/highscopeus"><img src="/wp-content/themes/highscope/assets/img/twitter.svg"><span>Twitter</span></a></li>
                                    <li><a href="https://www.instagram.com/highscopeus/"><img src="/wp-content/themes/highscope/assets/img/instagram.svg"><span>Instagram</span></a></li>
                                    <li><a href="https://www.linkedin.com/company/highscopeus"><img src="/wp-content/themes/highscope/assets/img/linkedin.svg"> <span>LinkedIn</span></a></li>
            </ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-6" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">SUPPORT OUR MISSION</h4><div class="textwidget custom-html-widget"><p>
                Together we can empower educators, lift families, and prepare our youngest children for school and for life.
            </p>
            <p>
                <a class="btn" href="/invest">Invest</a>
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CUSTOMER SERVICE</h4><div class="textwidget custom-html-widget"><ul class="footer-links"> 
    <li><a href="https://highscope.org/who-we-are/">About Us</a></li>
    <li><a href="/faq/">FAQ</a></li>
    <li><a href="/policies/">Terms & Policies</a></li>
            </ul></div></div><!-- end .fwidget --></div><div class="footer-widget"><div id="custom_html-9" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">PROFILE AND ORDERS</h4>
            <div class="textwidget custom-html-widget">
            	<ul class="footer-links">
            	<?php if(is_user_logged_in()){ ?>	   
					    <li><a href="/my-account/">My Account</a></li>
					    <li><a href="/cart/">My Cart</a></li>
					    <li><a href="#">Order Tracking</a></li>
					    <li><a href="#">My Wishlist</a></li>
					    <li><a href="/my-account/edit-account/">My Profile</a></li>
				<?php }else{?>	
						<li><a href="/my-account/">My Account</a></li>
				<?php } ?>		    
                    </ul>
             </div>
            </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-7" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CONTACT US</h4><div class="textwidget custom-html-widget"><p class="img-box with-icon location-icon">
<!--                    <img src="/wp-content/uploads/2021/07/ic-location.png">-->
                    600 North River Street<br>
                Ypsilanti, MI 48198-2898 USA</p>
                    <a href="mailto:hscst@highscope.org" class="img-box with-icon mail-icon">
<!--                        <img src="/wp-content/uploads/2021/07/ic-email.png">-->
                        hscst@highscope.org</a>
            <a href="tel:800.587.5639" class="with-icon phone-icon img-box">
<!--                <img src="/wp-content/uploads/2021/07/ic-call.png">-->
                800.587.5639
                    </a>
<!--                <a class="btn" href="mailto:hscst@highscope.org">Contact Us</a>-->
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->    </div> <!-- #footer-widgets -->
            </div>    <!-- .container -->
            <div id="footer-bottom">
					<div class="container clearfix">
										<div class="footer-logo">
                            <div class="img-box">
							<img alt="HighScope" src="/wp-content/uploads/2018/03/highscope-white.png" class="lazyload"/>
                                            </div> <span>Changing the trajectory of the world, one child at a time</span>
						</div>
						<p class="copyright">
							Copyright © 2021 HighScope Educational Research Foundation. The name "HighScope" and its corporate logos are registered trademarks and service marks of the HighScope Foundation.							
						</p>
					</div>	<!-- .container -->
				</div>
        </footer>
    </div>
</div>
<?php wp_footer(); ?>
<?php 
	$c = $_GET['c']; 
	$parent_one = get_ancestors($c, 'product_cat');
    if(!empty($parent_one)){
    	$c = $parent_one[0];
        $parent_two = get_ancestors($parent_one[0], 'product_cat');
    }
    if(!empty($parent_two)){
    	$c = $parent_two[0];
        $cates_array['first'] = $parent_two[0]; 
    }
?>
<script type="application/javascript">
    jQuery( ".img-box img" ).each(function( index ) {  jQuery( this ).attr('src',  jQuery( this ).attr('data-src')).removeClass( "lazyload" )  });
    setTimeout(function(){jQuery( ".addSpace" ).text("|") },1000);
    
    jQuery(".gridlist-toggle a").click(function(e) {
        e.preventDefault();
        jQuery( 'ul.products' ).removeClass("grid list");
        jQuery( 'ul.products' ).addClass( jQuery(this).attr("id"));
        jQuery(".gridlist-toggle a").removeClass( 'active' );
        jQuery(this).addClass( 'active' );
    });

    jQuery('li.blue-txt').parents('li.cat-parent').addClass('active');
    
    jQuery(document).ready(function(){
    	var _clss = "<?php echo $c; ?>";
    	jQuery('#top-menu').find('li.l_'+_clss).addClass('active');
    });	
</script>
</body>
</html>
