<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>
    <?php if(is_product() || is_cart() || is_checkout() || is_account_page()){ ?>
<div class="page-common small-footer">
<footer id="main-footer">		
            <div class="container">
                <div id="footer-widgets" class="clearfix">
                    <div class="footer-widget"><div id="custom_html-2" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Join Our Mailing List</h4><div class="textwidget custom-html-widget"><p>
                Be the first to learn about new strategies, products, and events.
            </p>
            <p>
                <a class="btn" href="https://signup.e2ma.net/signup/29012/17220/" onclick="window.open('https://signup.e2ma.net/signup/29012/17220/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up</a>
            </p>
            </div></div>
<div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Stay Connected</h4><div class="textwidget custom-html-widget"><ul class="social">
                                    <li><a href="https://www.facebook.com/HighScopeUS"> 
                                        <img src="/wp-content/themes/highscope/assets/img/facebook.svg"><span>Facebook</span></a></li>
                                    <li><a href="https://twitter.com/highscopeus"><img src="/wp-content/themes/highscope/assets/img/twitter.svg"><span>Twitter</span></a></li>
                                    <li><a href="https://www.instagram.com/highscopeus/"><img src="/wp-content/themes/highscope/assets/img/instagram.svg"><span>Instagram</span></a></li>
                                    <li><a href="https://www.linkedin.com/company/highscopeus"><img src="/wp-content/themes/highscope/assets/img/linkedin.svg"> <span>LinkedIn</span></a></li>
            </ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-6" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">SUPPORT OUR MISSION</h4><div class="textwidget custom-html-widget"><p>
                Together we can empower educators, lift families, and prepare our youngest children for school and for life.
            </p>
            <p>
                <a class="btn" href="/invest">Invest</a>
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CUSTOMER SERVICE</h4><div class="textwidget custom-html-widget"><ul class="footer-links"> 
    <li><a href="https://highscope.org/who-we-are/">About Us</a></li>
    <li><a href="/faq/">FAQ</a></li>
    <li><a href="/policies/">Terms & Policies</a></li>
            </ul></div></div><!-- end .fwidget --></div><div class="footer-widget"><div id="custom_html-9" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">PROFILE AND ORDERS</h4>
            <div class="textwidget custom-html-widget">
            	<ul class="footer-links">
            	<?php if(is_user_logged_in()){ ?>	   
					    <li><a href="/my-account/">My Account</a></li>
					    <li><a href="/cart/">My Cart</a></li>
					    <li><a href="#">Order Tracking</a></li>
					    <li><a href="#">My Wishlist</a></li>
					    <li><a href="/my-account/edit-account/">My Profile</a></li>
				<?php }else{?>	
						<li><a href="/my-account/">My Account</a></li>
				<?php } ?>		    
                    </ul>
             </div>
            </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-7" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CONTACT US</h4><div class="textwidget custom-html-widget"><p class="img-box with-icon location-icon">
<!--                    <img src="/wp-content/uploads/2021/07/ic-location.png">-->
                    600 North River Street<br>
                Ypsilanti, MI 48198-2898 USA</p>
                    <a href="mailto:hscst@highscope.org" class="img-box with-icon mail-icon">
<!--                        <img src="/wp-content/uploads/2021/07/ic-email.png">-->
                        hscst@highscope.org</a>
            <a href="tel:800.587.5639" class="with-icon phone-icon img-box">
<!--                <img src="/wp-content/uploads/2021/07/ic-call.png">-->
                800.587.5639
                    </a>
<!--                <a class="btn" href="mailto:hscst@highscope.org">Contact Us</a>-->
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->    </div> <!-- #footer-widgets -->
            </div>    <!-- .container -->
            <div id="footer-bottom">
					<div class="container clearfix">
										<div class="footer-logo">
                            <div class="img-box">
							<img alt="HighScope" src="/wp-content/uploads/2018/03/highscope-white.png" class="lazyload"/>
                                            </div> <span>Changing the trajectory of the world, one child at a time</span>
						</div>
						<p class="copyright">
							Copyright © 2021 HighScope Educational Research Foundation. The name "HighScope" and its corporate logos are registered trademarks and service marks of the HighScope Foundation.							
						</p>
					</div>	<!-- .container -->
				</div>
        </footer>
</div>
<?php } else { ?>
			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}
				?>
						<div class="footer-logo">
							<img src="/wp-content/uploads/2018/03/highscope-white.png" alt="HighScope" /> <span>Changing the trajectory of the world, one child at a time</span>
						</div>
						<p class="copyright">
							Copyright &copy; <?php echo date("Y"); ?> HighScope Educational Research Foundation. The name "HighScope" and its corporate logos are registered trademarks and service marks of the HighScope Foundation.<br/>
							<a href="https://boxcarstudio.com">Michigan Web Design by Boxcar Studio</a>
						</p>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
<?php } ?>
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
<script>
    jQuery('<div class="product-banner"><div class="container"><div class="row"><div class="col-12"></div></div></div></div>').insertBefore('#main-content > .container');
    jQuery('.single-product .product-banner .col-12').append(jQuery('.woocommerce-breadcrumb'));
    jQuery('.woocommerce-account .product-banner .col-12, .woocommerce-cart .product-banner .col-12').append(jQuery('h1.entry-title.main_title,h1.entry-login'));
</script>
</body>
</html>