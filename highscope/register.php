<?php 
	/* Template Name: registration  */ 
	get_header(); 
?> 
<?php
if(isset($_POST['register'])){
    global $reg_errors;
    global $wpdb;
    $tablename = $wpdb->prefix.'taxjar_entries';
    $reg_errors = new WP_Error;
    
    $title = $_POST['title'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $company = $_POST['company'];
    $mailid  = $_POST['emails'];
    $password = $_POST['passwords'];
    $username = strstr($mailid, '@', true);
    $address = $_POST['address'];
    $address2 = $_POST['address2'];
    $city = $_POST['city'];
    $country = $_POST['country'];
    $state = $_POST['state'];
    $zipcode = $_POST['zipcode'];
    $phone = $_POST['phone'];
    $fax = $_POST['fax'];
    
    /*********** Validation  ********************/
    if ( email_exists( $mailid ) )
    {	
        $reg_errors->add( 'email', 'email already exist!' );
    }
    
    if (is_wp_error( $reg_errors ))
    { 
        foreach ( $reg_errors->get_error_messages() as $error )
        {
             $signUpError='<strong>ERROR</strong>: '.$error;
        } 
    }
/*******************************Validation end here ***********************/
    
    if ( 1 > count( $reg_errors->get_error_messages() ) ){
        global $username, $useremail;
        $username   =   sanitize_user( $username );
        $useremail  =   sanitize_email( $_POST['emails'] );
        $password   =   esc_attr( $_POST['passwords'] );
        $userdata = array(
            'user_login'    =>   $username,
            'user_email'    =>   $useremail,
            'user_pass'     =>   $password,
            'user_registered' => date('Y-m-d H:i:s'),
            );
        $user_ids = wp_insert_user($userdata);

        update_user_meta( $user_ids, 'title', $title);
        update_user_meta( $user_ids, 'first_name', $first_name);
        update_user_meta( $user_ids, 'last_name', $last_name);
        update_user_meta( $user_ids, 'billing_company', $company);
        update_user_meta( $user_ids, 'billing_address_1', $address);
        update_user_meta( $user_ids, 'billing_address_2', $address2);
        update_user_meta( $user_ids, 'billing_city', $city);
        update_user_meta( $user_ids, 'billing_postcode', $zipcode);
        update_user_meta( $user_ids, 'billing_state', $state);
        update_user_meta( $user_ids, 'billing_phone', $phone);
        update_user_meta( $user_ids, 'fax', $fax);
        send_email_user_registration($first_name, $last_name, $mailid);

        if(!empty($_POST['exempt_type'])){
	    	$user_id = $user_ids;
	        $posts = $user_ids;
	        
	        $bemail = $useremail;
	        $names = $first_name." ".$last_name;
	        $exempt_type = $_POST['exempt_type'];
	        $exempt_states = implode(',', $_POST['exempt_states']);
	        $certificate_expire = $_POST['certificate_expire'];
	        $tax_certificate = '';
	        if (!empty($_POST['pdf_base649'] )) {
	            $tax_certificate = save_pdf_canvas($posts, $_POST['pdf_base649'],'certificate_'.time());
	        }
	        
	        $query = "INSERT INTO $tablename (user_id, user_name, user_email, exempt_type, exempt_states, tax_certificate, certificate_expire, status) VALUES ('$user_id','$names','$bemail','$exempt_type','$exempt_states','$tax_certificate','$certificate_expire','3')";
		                $wpdb->query($query);    

	        update_user_meta($user_ids, 'tex_exempt', 1); 
	        send_email_text_exemptions($names, $bemail, $exempt_type, $exempt_states, $tax_certificate, $certificate_expire);
	    }
        $success ="You have successfully registered.";
	
    }

}

?>
  <div id="main-content" tabindex="-1" role="main">
      <div class="product-banner"><div class="container"><div class="row"><div class="col-12"><h1 class="entry-title main_title">Sign up</h1></div></div></div></div>

	<div class="container">
		<div id="content-area" class="clearfix dds">
			<div id="left-area">


			
				<article id="post-77188" class="post-77188 page type-page status-publish hentry pmpro-has-access">

				
				
					<div class="entry-content">
					<div class="woocommerce"><div class="woocommerce-notices-wrapper"></div>

		<?php
		if(isset($signUpError)){echo '<p class="erro-msg">'.$signUpError.'</p>';}
		if($success){
			echo '<p class="suceess">'.$success.'</p>';
		}
		?>
		<form class="woocommerce-form woocommerce-form-registrations" method="post" autocomplete="off">
            <div class="row">
                <div class="col-3">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Title</label>
                        <select name="title" class="woocommerce-select">
                            <option value=""> - </option>
                            <option value="DR.">DR.</option>
                            <option value="MR.">MR.</option>
                            <option value="MRS.">MRS.</option>
                            <option value="MS.">MS.</option>
                        </select>
                    </p>
                </div>
                <div class="col-3">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Language</label>
                        <select name="language" class="woocommerce-select">
                            <option value="ENGLISH">ENGLISH</option>

                        </select>
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Company</label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="company" id="company" value="">
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">First Name<span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="first_name" id="first_name" value="">
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Last Name<span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="last_name" id="last_name" value="">
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Email<span class="required">*</span></label>
                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="emails" id="emails" value="">
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Confirm Email<span class="required">*</span></label>
                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="confirm_email" id="confirm_email" value="">
                    </p>
                </div>
            </div>
             <div class="row">
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Password<span class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="passwords" id="passwords" value="" autocomplete="false">
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Confirm Password<span class="required">*</span></label>
                        <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="cpassword" id="cpassword" value="">
                    </p>
                </div>
            </div>
            
            
            <p class="form-title">MY ADDRESS</p>
            <div class="row">
                <div class="col-12">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Address<span class="required">*</span></label>
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="address" id="address" value="" placeholder="Address 1">
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="address2" id="address2" value=""  placeholder="Address 2">
                    </p>
                </div>
            </div>
            <div class="row four-col">
                <div class="col-3">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Country<span class="required">*</span></label>
                        <select name="country" class="woocommerce-select">
                            <option selected="selected" value="US">United States</option>
                        </select>
                    </p>
                </div>
                <div class="col-3">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="username">Province/State/Region<span class="required">*</span></label>
                    <select name="state" class="woocommerce-select">
                        <option value="US-AL">Alabama</option>
                        <option value="US-AK">Alaska</option>
                        <option value="US-AS">American Samoa</option>
                        <option value="US-AZ">Arizona</option>
                        <option value="US-AR">Arkansas</option>
                        <option value="US-CA">California</option>
                        <option value="US-CO">Colorado</option>
                        <option value="US-CT">Connecticut</option>
                        <option value="US-DE">Delaware</option>
                        <option value="US-DC">District Of Columbia</option>
                        <option value="US-FL">Florida</option>
                        <option value="US-GA">Georgia</option>
                        <option value="US-GU">Guam</option>
                        <option value="US-HI">Hawaii</option>
                        <option value="US-ID">Idaho</option>
                        <option value="US-IL">Illinois</option>
                        <option value="US-IN">Indiana</option>
                        <option value="US-IA">Iowa</option>
                        <option value="US-KS">Kansas</option>
                        <option value="US-KY">Kentucky</option>
                        <option value="US-LA">Louisiana</option>
                        <option value="US-ME">Maine</option>
                        <option value="US-MD">Maryland</option>
                        <option value="US-MA">Massachusetts</option>
                        <option selected="selected" value="US-MI">Michigan</option>
                        <option value="US-MN">Minnesota</option>
                        <option value="US-MS">Mississippi</option>
                        <option value="US-MO">Missouri</option>
                        <option value="US-MT">Montana</option>
                        <option value="US-NE">Nebraska</option>
                        <option value="US-NV">Nevada</option>
                        <option value="US-NH">New Hampshire</option>
                        <option value="US-NJ">New Jersey</option>
                        <option value="US-NM">New Mexico</option>
                        <option value="US-NY">New York</option>
                        <option value="US-NC">North Carolina</option>
                        <option value="US-ND">North Dakota</option>
                        <option value="US-MP">Northern Mariana Islands</option>
                        <option value="US-OH">Ohio</option>
                        <option value="US-OK">Oklahoma</option>
                        <option value="US-OR">Oregon</option>
                        <option value="US-PA">Pennsylvania</option>
                        <option value="US-PR">Puerto Rico</option>
                        <option value="US-RI">Rhode Island</option>
                        <option value="US-SC">South Carolina</option>
                        <option value="US-SD">South Dakota</option>
                        <option value="US-TN">Tennessee</option>
                        <option value="US-TX">Texas</option>
                        <option value="US-UM">U.S. Minor Outlying Islands</option>
                        <option value="US-UT">Utah</option>
                        <option value="US-VT">Vermont</option>
                        <option value="US-VI">Virgin Islands Of The U.S.</option>
                        <option value="US-VA">Virginia</option>
                        <option value="US-WA">Washington</option>
                        <option value="US-WV">West Virginia</option>
                        <option value="US-WI">Wisconsin</option>
                        <option value="US-WY">Wyoming</option>
                    </select>
			      </p>
                </div>
                <div class="col-3">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">City<span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="city" id="city" value="">
                    </p>
                </div>
                <div class="col-3">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Zipcode<span class="required">*</span></label>
                        <input type="number" class="woocommerce-Input woocommerce-Input--text input-text" name="zipcode" id="zipcode" value="">
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Phone<span class="required">*</span></label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="phone" id="phone" value="" autocomplete="false">
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <label for="username">Fax</label>
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="fax" id="fax" value="">
                    </p>
                </div>
            </div>
            
            
            <p class="form-title">SALES TAX EXEMPTION</p>
            <div class="row">
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--first form-row">
                        <label for="account_first_name">Exemption Type&nbsp;</label>
                        <select class="woocommerce-select" name="exempt_type">
                            <option value="">Non-Exempt</option>
                            <option value="wholesale">Wholesale / Resale</option>
                            <option value="government">Government</option>
                            <option value="other">Other</option>
                        </select>    
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--last form-row">
                        <label for="account_last_name">Exempt States (Can choose multiple states)&nbsp;</label>
                        <select name="exempt_states[]" class="multipels woocommerce-Input woocommerce-Input--text input-text" multiple>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select> 
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--first form-row ">
                        <label for="account_first_name">Tax Certificate&nbsp;</label>
                        <input type="file" id="files" name="tax_certificate" accept="application/pdf"/>
                         <input id="pdf_base649" name="pdf_base649" type="hidden" value="" />
                    </p>
                </div>
                <div class="col-6">
                    <p class="woocommerce-form-row woocommerce-form-row--first form-row">
                        <label for="account_first_name">Certificate	Expiration&nbsp;</label>
                        <input type="date" class="woocommerce-Input woocommerce-Input--text input-text" name="certificate_expire" value="" />   
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p class="form-row mt-4">
                        <button type="submit" class="woocommerce-button button register" name="register" value="register">Sign Up</button>
                    </p>
                    <?php echo do_shortcode('[bws_google_captcha]'); ?>	
                </div>
                <div class="col-12">
                    <p class="woocommerce-LostPassword lost_password mt-4">
                        Already have an account? <a href="/my-account/">Sign In</a>
                    </p>
                </div>
            </div>
		</form>


</div>				
			
					</div> <!-- .entry-content -->

				
				</article> <!-- .et_pb_post -->

			

			</div> <!-- #left-area -->

					</div> <!-- #content-area -->
	</div> <!-- .container -->


</div>

       <div class="page-common small-footer"> 
           <footer id="main-footer">		
            <div class="container">
                <div id="footer-widgets" class="clearfix">
                    <div class="footer-widget"><div id="custom_html-2" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Join Our Mailing List</h4><div class="textwidget custom-html-widget"><p>
                Be the first to learn about new strategies, products, and events.
            </p>
            <p>
                <a class="btn" href="https://signup.e2ma.net/signup/29012/17220/" onclick="window.open('https://signup.e2ma.net/signup/29012/17220/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up</a>
            </p>
            </div></div>
<div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Stay Connected</h4><div class="textwidget custom-html-widget"><ul class="social">
                                    <li><a href="https://www.facebook.com/HighScopeUS"> 
                                        <img src="/wp-content/themes/highscope/assets/img/facebook.svg"><span>Facebook</span></a></li>
                                    <li><a href="https://twitter.com/highscopeus"><img src="/wp-content/themes/highscope/assets/img/twitter.svg"><span>Twitter</span></a></li>
                                    <li><a href="https://www.instagram.com/highscopeus/"><img src="/wp-content/themes/highscope/assets/img/instagram.svg"><span>Instagram</span></a></li>
                                    <li><a href="https://www.linkedin.com/company/highscopeus"><img src="/wp-content/themes/highscope/assets/img/linkedin.svg"> <span>LinkedIn</span></a></li>
            </ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-6" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">SUPPORT OUR MISSION</h4><div class="textwidget custom-html-widget"><p>
                Together we can empower educators, lift families, and prepare our youngest children for school and for life.
            </p>
            <p>
                <a class="btn" href="/invest">Invest</a>
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CUSTOMER SERVICE</h4><div class="textwidget custom-html-widget"><ul class="footer-links"> 
    <li><a href="https://highscope.org/who-we-are/">About Us</a></li>
    <li><a href="/faq/">FAQ</a></li>
    <li><a href="/policies/">Terms & Policies</a></li>
            </ul></div></div><!-- end .fwidget --></div><div class="footer-widget"><div id="custom_html-9" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">PROFILE AND ORDERS</h4>
            <div class="textwidget custom-html-widget">
            	<ul class="footer-links">
            	<?php if(is_user_logged_in()){ ?>	   
					    <li><a href="/my-account/">My Account</a></li>
					    <li><a href="/cart/">My Cart</a></li>
					    <li><a href="#">Order Tracking</a></li>
					    <li><a href="#">My Wishlist</a></li>
					    <li><a href="/my-account/edit-account/">My Profile</a></li>
				<?php }else{?>	
						<li><a href="/my-account/">My Account</a></li>
				<?php } ?>		    
                    </ul>
             </div>
            </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-7" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CONTACT US</h4><div class="textwidget custom-html-widget"><p class="img-box with-icon location-icon">
<!--                    <img src="/wp-content/uploads/2021/07/ic-location.png">-->
                    600 North River Street<br>
                Ypsilanti, MI 48198-2898 USA</p>
                    <a href="mailto:hscst@highscope.org" class="img-box with-icon mail-icon">
<!--                        <img src="/wp-content/uploads/2021/07/ic-email.png">-->
                        hscst@highscope.org</a>
            <a href="tel:800.587.5639" class="with-icon phone-icon img-box">
<!--                <img src="/wp-content/uploads/2021/07/ic-call.png">-->
                800.587.5639
                    </a>
<!--                <a class="btn" href="mailto:hscst@highscope.org">Contact Us</a>-->
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->    </div> <!-- #footer-widgets -->
            </div>    <!-- .container -->
            <div id="footer-bottom">
					<div class="container clearfix">
										<div class="footer-logo">
                            <div class="img-box">
							<img alt="HighScope" src="/wp-content/uploads/2018/03/highscope-white.png" class="lazyload"/>
                                            </div> <span>Changing the trajectory of the world, one child at a time</span>
						</div>
						<p class="copyright">
							Copyright © 2021 HighScope Educational Research Foundation. The name "HighScope" and its corporate logos are registered trademarks and service marks of the HighScope Foundation.							
						</p>
					</div>	<!-- .container -->
				</div>
        </footer>
       </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>

