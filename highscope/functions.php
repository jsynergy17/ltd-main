<?php
function my_theme_enqueue_styles() { 
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


// Redirect on login to Membership Account page
/*add_filter( 'login_redirect', function( $url, $query, $user ) {
	return '/membership-account';
}, 10, 3 );
*/
//Custom formstack shortcodes
//[formstack-keep-abreast]
function formstack_keep_abreast_function() {
     return '<script type="text/javascript" src="https://HighScope.formstack.com/forms/js.php/keep_abreast"></script><noscript><a href="https://HighScope.formstack.com/forms/keep_abreast" title="Online Form">Online Form - Keep Abreast of Anniversary Activities & Events</a></noscript><div style="text-align:right; font-size:x-small;"></div>';
}
add_shortcode('formstack-keep-abreast', 'formstack_keep_abreast_function');

//[formstack-annual-giving]
function formstack_annual_giving_function() {
     return '<script type="text/javascript" src="https://HighScope.formstack.com/forms/js.php/annual_giving"></script><noscript><a href="https://HighScope.formstack.com/forms/annual_giving" title="Online Form">Online Form - Annual Giving </a></noscript>';
}
add_shortcode('formstack-annual-giving', 'formstack_annual_giving_function');

//[formstack-tickets]
function formstack_tickets_function() {
     return '<script type="text/javascript" src="https://HighScope.formstack.com/forms/js.php/tickets"></script><noscript><a href="https://HighScope.formstack.com/forms/tickets" title="Online Form">Online Form - Gala Tickets Order Form</a></noscript><div style="text-align:right; font-size:x-small;"></div>';
}
add_shortcode('formstack-tickets', 'formstack_tickets_function');

//[formstack-sponsors]
function formstack_sponsors_function() {
     return '<script type="text/javascript" src="https://HighScope.formstack.com/forms/js.php/sponsors"></script><noscript><a href="https://HighScope.formstack.com/forms/sponsors" title="Online Form">Online Form - Gala Sponsorship Options</a></noscript><div style="text-align:right; font-size:x-small;"></div>';
}
add_shortcode('formstack-sponsors', 'formstack_sponsors_function');

//[formstack-auction-donations]
function formstack_auction_donations_function() {
     return '<script type="text/javascript" src="https://HighScope.formstack.com/forms/js.php/auction_donations"></script><noscript><a href="https://HighScope.formstack.com/forms/auction_donations" title="Online Form">Online Form - Gala Auction Donations</a></noscript><div style="text-align:right; font-size:x-small;"></div>';
}
add_shortcode('formstack-auction-donations', 'formstack_auction_donations_function');


/* Using email address as username for signups */
add_action('login_head', function(){
?>
    <style>
        #registerform > p:first-child{
            display:none;
        }
    </style>

    <script type="text/javascript" src="<?php echo site_url('/wp-includes/js/jquery/jquery.js'); ?>"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#registerform > p:first-child').css('display', 'none');
        });
    </script>
<?php
});
//Remove error for username, only show error for email only.
add_filter('registration_errors', function($wp_error, $sanitized_user_login, $user_email){
    if(isset($wp_error->errors['empty_username'])){
        unset($wp_error->errors['empty_username']);
    }

    if(isset($wp_error->errors['username_exists'])){
        unset($wp_error->errors['username_exists']);
    }
    return $wp_error;
}, 10, 3);
add_action('login_form_register', function(){
    if(isset($_POST['user_login']) && isset($_POST['user_email']) && !empty($_POST['user_email'])){
        $_POST['user_login'] = $_POST['user_email'];
    }
});

/* end of email as username */

/* woocommerce image sizing */
add_theme_support( 'woocommerce', array(
'single_image_width' => 200,
) );

/* end image sizing */


/* Enable Divi Builder on all post types with an editor box */
function myprefix_add_post_types($post_types) {
	foreach(get_post_types() as $pt) {
		if (!in_array($pt, $post_types) and post_type_supports($pt, 'editor')) {
			$post_types[] = $pt;
		}
	} 
	return $post_types;
}
add_filter('et_builder_post_types', 'myprefix_add_post_types');

/* Add Divi Custom Post Settings box */
function myprefix_add_meta_boxes() {
	foreach(get_post_types() as $pt) {
		if (post_type_supports($pt, 'editor') and function_exists('et_single_settings_meta_box')) {
			add_meta_box('et_settings_meta_box', __('Divi Custom Post Settings', 'Divi'), 'et_single_settings_meta_box', $pt, 'side', 'high');
		}
	} 
}
add_action('add_meta_boxes', 'myprefix_add_meta_boxes');

/* Ensure Divi Builder appears in correct location */
function myprefix_admin_js() { 
	$s = get_current_screen();
	if(!empty($s->post_type) and $s->post_type!='page' and $s->post_type!='post') { 
?>

<?php
	}
}
add_action('admin_head', 'myprefix_admin_js');

// Ensure that Divi Builder framework is loaded - required for some post types when using Divi Builder plugin
add_filter('et_divi_role_editor_page', 'myprefix_load_builder_on_all_page_types');
function myprefix_load_builder_on_all_page_types($page) { 
	return isset($_GET['page'])?$_GET['page']:$page; 
}

/*********** Custom Login ********************/
function custom_login_page() {
echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
}
add_action('login_head', 'custom_login_page');

/*********** Child theme creation ********************/
function divi_child_theme_setup() {
    if ( ! class_exists('ET_Builder_Module') ) {
        return;
    }
    get_template_part( 'custom-modules/cfwpm' );
    $cfwpm = new Custom_ET_Builder_Module_Login();
    remove_shortcode( 'et_pb_login' );   
    add_shortcode( 'et_pb_login', array($cfwpm, '_shortcode_callback') );
}
add_action( 'wp', 'divi_child_theme_setup', 9999 );


/*********** Change the breadcrumb separator ********************/

add_filter( 'woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter' );
function wcc_change_breadcrumb_delimiter( $defaults ) {
	$defaults['delimiter'] = '<span class="addSpace">&#9632;</span>';
	return $defaults;
}


/*********** Function for extra pegination options ********************/
function extra_pagination_options($output, $grid_view, $list_view ) {
    $output = sprintf( '<nav class="gridlist-toggle">Display Type <a href="#" id="grid" title="%1$s"><span class="dashicons dashicons-grid-view"></span> <em>%1$s</em></a><a href="#" id="list" title="%2$s"><span class="dashicons dashicons-exerpt-view"></span> <em>%2$s</em></a></nav>', $grid_view, $list_view );

    $seleted1 = $seleted2 = $seleted3 = ''; 
    
    if($_GET['per_page'] == 24){
        $seleted1 = 'selected';
    }else if($_GET['per_page'] == 48){
        $seleted2 = 'selected';
    }else if($_GET['per_page'] == 72){
        $seleted3 = 'selected';
    }

    $output .='<div class="result_per_page">
        <span class="msg">Results per page</span>
        <select name="per_page_data" class="per_page_data">
            <option value="24" '.$seleted1.'>24</option>
            <option value="48" '.$seleted2.'>48</option>
            <option value="72" '.$seleted3.'>72</option>
        </select>
        </div>';
    return $output;

}
add_filter( 'gridlist_toggle_button_output', 'extra_pagination_options', 10,3);


/*********** Function sorting options ********************/
function new_loop_shop_per_page( $cols ) {
    $cols = 24;
    if($_GET['per_page']){
        $cols = $_GET['per_page'];
    }
    return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );


/*********** Function for add new js on admin ********************/
function admin_enqueue_scriptsss() {
    wp_enqueue_script(
        'sweet-alert','https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js',
        array( 'jquery' )
    );     
}
/*********** Function for add new file on child theme ********************/
function add_child_theme_js_css() {
    wp_enqueue_script(
        'sweet-alert','https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js',
        array( 'jquery' )
    );  
    wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/assets/js/custom_script.js',
        array( 'jquery' ));
    
}
add_action( 'wp_enqueue_scripts', 'add_child_theme_js_css', 11);
add_action( 'admin_enqueue_scripts', 'admin_enqueue_scriptsss', 11);



/*********** Function for sorting with alpthbatically ********************/
function sip_alphabetical_shop_ordering( $sort_args ) {
    $orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
    if ( 'code' == $orderby_value ) {
        $sort_args['orderby'] = 'title';
        $sort_args['order'] = 'asc';
        $sort_args['meta_key'] = '';
    }

    if ( 'title' == $orderby_value ) {
        $sort_args['orderby'] = 'title';
        $sort_args['order'] = 'asc';
        $sort_args['meta_key'] = '';
    }

    if ( 'brand' == $orderby_value ) {
        $sort_args['orderby'] = 'title';
        $sort_args['order'] = 'asc';
        $sort_args['meta_key'] = '';
    }
    return $sort_args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'sip_alphabetical_shop_ordering' );

/*********** Function for add sorting  ********************/
function sip_custom_wc_catalog_orderby( $sortby ) {
    $sortby['menu_order'] = 'OUR PICKS';
    $sortby['popularity'] = 'TOP SELLERS';
    $sortby['price'] = 'PRICE RETAIL (LOWEST FIRST)';
    $sortby['price-desc'] = 'PRICE RETAIL (HIGHEST FIRST)';
    $sortby['code'] = 'CODE';
    $sortby['title'] = 'TITLE';
    $sortby['brand'] = 'BRAND';
    return $sortby;
}
add_filter( 'woocommerce_default_catalog_orderby_options', 'sip_custom_wc_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'sip_custom_wc_catalog_orderby' );


/*********** function for add extra fields on checkout for po********************/

add_action( 'woocommerce_after_checkout_billing_form', 'display_extra_fields_after_billing_address' , 10, 1 );

function display_extra_fields_after_billing_address () {

     _e( "PO File ", "add_extra_fields");
     ?>
     <br>
     <style>span.optional {display: none;}</style>
     <input id="files" name="inputFile" type="file"/>
     <input id="pdf_base64" name="pdf_base64" type="hidden" />
     <script type="text/javascript">
     	document.getElementById('files').addEventListener('change', handleFileSelect, false);
		function handleFileSelect(evt) {
			var f = evt.target.files[0]; // FileList object
			if(f.type == 'application/pdf'){
				var reader = new FileReader();
				reader.onload = (function(theFile) {
				return function(e) {
					var binaryData = e.target.result;
					var base64String = window.btoa(binaryData);
					document.getElementById('pdf_base64').value = base64String;
				};
				})(f);
				reader.readAsBinaryString(f);
			}else{
				alert('Please select pdf file');
				document.getElementById('files').value=''
			}	
		}
</script><br>
     
     <?php 
}

/*********** function for add extra fields update ********************/
add_action( 'woocommerce_checkout_update_order_meta', 'add_order_po_file_to_order' , 10, 1);
function add_order_po_file_to_order ( $order_id ) {
	global $wpdb;
    $tablename = $wpdb->prefix.'taxjar_entries';
	 if (!empty($_POST['pdf_base64'] )) {
	 	$pdf = save_pdf_canvas($order_id, $_POST['pdf_base64'], 'po_');
	    add_post_meta( $order_id, 'po_file', $pdf);
	 }

	 if (!empty($_POST['fname'] )) {
	    add_post_meta( $order_id, 'fname', $_POST['fname']);
	    add_post_meta( $order_id, 'lname', $_POST['lname']);
	    add_post_meta( $order_id, 'ename', $_POST['ename']);
	 }

	 if(!empty($_POST['tax_exempts'])){
	 	$user_id = '';
    	$posts = time();
    	if(is_user_logged_in()){
        	$user_id = get_current_user_id();
        	$posts = $user_id;
        }
        
        $bemail = $_POST['billing_email'];
        $names = $_POST['billing_first_name']." ".$_POST['billing_last_name'];
        $exempt_type = $_POST['exempt_type'];
        $exempt_states = implode(',', $_POST['exempt_states']);
        $certificate_expire = $_POST['certificate_expire'];
        $tax_certificate = '';
        if (!empty($_POST['pdf_base649'] )) {
            $tax_certificate = save_pdf_canvas($posts, $_POST['pdf_base649'],'certificate_'.time());
        }
        
        $query = "INSERT INTO $tablename (user_id, user_name, user_email, exempt_type, exempt_states, tax_certificate, certificate_expire, status) VALUES ('$user_id','$names','$bemail','$exempt_type','$exempt_states','$tax_certificate','$certificate_expire','3')";
	                $wpdb->query($query);    

        if(is_user_logged_in()){
        	update_user_meta(get_current_user_id(), 'tex_exempt', 1); 
        }
        send_email_text_exemptions($names, $bemail, $exempt_type, $exempt_states, $tax_certificate, $certificate_expire);
        add_post_meta( $order_id, 'tex_exempt_order', 1);
    }    
}


/*********** function for add extra fields  ********************/
add_action( 'woocommerce_after_order_itemmeta', 'action_woocommerce_after_order_itemmeta', 10, 3 );
function action_woocommerce_after_order_itemmeta( $item_id, $item, $null ) {
    $order_id = $_GET['post'];
    $po_file = get_post_meta( $order_id, 'po_file', true );
    $fname = get_post_meta( $order_id, 'fname', true );
    
    if($po_file){
    	echo '<p><strong>' . __( 'PO Files', 'add_extra_fields' ) . ':</strong> <a target="_blank" href="'.$po_file.'">Download</a>';
    }

    if(!empty($fname)){
    	$lname = get_post_meta( $order_id, 'lname', true );
    	$ename = get_post_meta( $order_id, 'ename', true );
    	echo '<div class="users_details"><p><b>Users Details</b></p>';
    	echo '<table style="width:100%" class="user_details"><tr><th>S.no.</th><th>First Name</th><th>Last Name</th><th>Email</th></tr>';
      	$counter = 0;
        foreach ($fname as $value) { ?>
			<tr>
				<td><?php echo ($counter + 1); ?></td>
				<td><?php echo $value; ?></td>
				<td><?php echo $lname[$counter]; ?></td>
				<td><?php echo $ename[$counter]; ?></td>
			</tr>
        <?php } 
        echo '</table></div><br/>';
        ?>
        <style>
        	#order_shipping_line_items .users_details {display: block;}
		</style>
    <?php }	
}

/*********** function for save pdf file  ********************/
function save_pdf_canvas($order_id, $base64_img, $names) {
    $image_data2 = base64_decode($base64_img);
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $filename2 = $names.$order_id.'.pdf';
    $file2 = $upload_dir . '/' . $filename2;
    file_put_contents( $file2, $image_data2 );
    return $custom_img2 = $upload['baseurl'] . '/'.$filename2;
  }


  add_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices2', 10 );

 /*********** function for add search option on shop page ********************/
 function woocommerce_output_all_notices2(){ 
 	echo do_shortcode('[smart_search id="1"]');
 }

 /*********** function for searching per post ********************/
 function myprefix_search_posts_per_page( $query) {

	if ( $query->is_search() && $query->is_main_query() ) {
		$query->set( 'posts_per_page', (isset($_GET['per_page']))?$_GET['per_page']:24);
	}
	return $query;
 }
 add_filter( 'pre_get_posts', 'myprefix_search_posts_per_page', 9999);


 /*********** function for add extra fields on product admin ********************/
add_action('woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields');
function woocommerce_product_custom_fields(){
    global $woocommerce, $post;
    echo '<div class="product_custom_field">';

    woocommerce_wp_text_input(
        array(
            'id' => 'custom_isbn',
            'placeholder' => 'ISBN Number',
            'label' => __('ISBN Number', 'woocommerce'),
            'desc_tip' => 'true'
        )
    );

   

    do_action( 'wcpf_add_variation_settings' );
    echo '</div>';
}

 /*********** function for save extra fields on product  ********************/
add_action('woocommerce_process_product_meta', 'woocommerce_product_custom_fields_save');
function woocommerce_product_custom_fields_save($post_id){
    $woocommerce_custom_product_text_field = $_POST['custom_isbn'];
    if (!empty($woocommerce_custom_product_text_field))
        update_post_meta($post_id, 'custom_isbn', esc_attr($woocommerce_custom_product_text_field));

    
}

/*********** function for show user info ********************/
add_action( 'woocommerce_after_checkout_billing_form', 'display_extra_fields_after_last' , 10, 1 );
function display_extra_fields_after_last () { 
    $cat_in_cart = false;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
       if ( has_term('421', 'product_tag', $cart_item['product_id'] ) ) {
         $cat_in_cart = true;
         break;
      }
    } 
    if ( $cat_in_cart ) { ?>
         <br>
         <div class="multiple">
         	<lable>Please provide the information below so we can set up your Letter Links account</lable>
         	<div class="inner_multiples initial">
    	     	<input class="repeated_unput" name="fname[]" type="text" placeholder="First Name" err_name="_fname-err" />
    	     	<input class="repeated_unput" name="lname[]" type="text" placeholder="Last Name" err_name="_lname-err"/>
    	     	<input class="repeated_unput" name="ename[]" type="text" placeholder="Email" err_name="_ename-err"/>
    	     	<span class="plus-add">+</span>
    	     	<span class="remove-minus">-</span>
         	</div>
        </div>
     <?php
    }  
}


/*********** function for add extra ISPN fields ********************/
 function my_custom_action() { 
    global $product;
    echo '<p  class="ispn_details">ISBN : '.get_post_meta($product->get_id(),'custom_isbn', true).'</p>';
    echo '<p  class="ispn_details">Product code : '.$product->get_sku().'</p>';    
 };     
 add_action( 'woocommerce_single_product_summary', 'my_custom_action', 10, 1);



/*********** function for cutom avalablity text change ********************/
add_filter( 'woocommerce_get_availability', 'custom_get_availability', 1, 2);
function custom_get_availability( $availability, $_product ) {
    global $product;
    $stock = $product->get_stock_quantity();
    if($stock > 0){
        $availability['availability'] = __('In stock', 'woocommerce');
    }else{
        $availability['availability'] = __('Out of Stock (Can be back-ordered)', 'woocommerce');
    } 
    return $availability;
}


/*********** function for validate letter link ********************/
add_action( 'woocommerce_after_checkout_validation', 'letter_link_validate_details', 10, 2);
function letter_link_validate_details( $fields, $errors ){
    if (isset($_POST['fname'])){
        $checker = 0;
        if(empty($_POST['fname'][0])){
            $checker = 1;
        }

        if(empty($_POST['lname'][0])){
            $checker = 1;
        }

        if(empty($_POST['ename'][0])){
            $checker = 1;
        }

        if($checker == 1){
            $errors->add( 'validation', 'Please enter letter links account details.' );
        }

        if(!empty($_POST['ename'][0])){
        	$flag = 0;
            foreach($_POST['ename'] as $values){
            	if(!valid_email($values)){
            		$flag = 1;
            	}
            }
            if($flag == 1){
            	$errors->add( 'validation', 'Please enter valid email of letter links account.' );
            }
        } 
    }

    if(!empty($_POST['tax_exempts'])){
        if(empty($_POST['exempt_type']) || empty($_POST['exempt_states']) || empty($_POST['pdf_base649']) || empty($_POST['certificate_expire'])){
            $errors->add( 'validation', 'Please enter tax exemption details.' );
        }
    }
}

/*********** function for shipping new text ********************/
add_action( 'woocommerce_after_shipping_rate', 'action_after_shipping_rate', 20, 2 );
function action_after_shipping_rate ( $method, $index ) {
    if( is_cart() ) return; // Exit on cart page

    if( 'table_rate:3:1' === $method->id ) {
        echo __(" <p class='font_400'>If you want expedited shipping or need international shipping, please fill in the information in the Order Notes box below and send to High Scope Customer support at <a href='tel:734-485-2000'>734-485-2000 ext 279 or 273.</a> and <a href='mailto:press@highscope.org'>press@highscope.org</a></p>");
    }
    
}

/*********** function for valida email ********************/
function valid_email($str) {
	return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? FALSE : TRUE;
}


/*********** function for send email for shipping ********************/
function send_mail_internation_ship(){
    global $woocommerce;
    $admin_email = get_option('admin_email');
    $name = $_POST['fname']."&nbsp;".$_POST['lname'];
    $location = $_POST['location'];
    $mail = $_POST['mail'];
    $phone = $_POST['billing_phone'];
    $address = $_POST['billing_address_1']."&nbsp;".$_POST['billing_city']."&nbsp;".$_POST['billing_postcode'];
    $subject = 'International Shipping';
    
    $items = $woocommerce->cart->get_cart();
    $product_data = '';
    foreach($items as $item => $values) { 
        $_product =  wc_get_product( $values['data']->get_id()); 
        $product_data .='<p>'.$_product->get_title().'</p>'; 
    } 

    $message = "<!doctype html><html lang='en'><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'><meta content='width=device-width' name='viewport'><title>Email</title></head><body style='margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #fff;'><table   style='width:480pt; padding: 11.25pt 0' ><tbody><tr><td  style='padding:0;'><table  ><tbody><tr><td  style='padding:7.5pt 3.75pt 7.5pt 7.5pt;'><p  style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/' target='_blank'><span style='color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo.png'  alt='HighScope Logo' style='width:292.19pt;height:77.4pt;'></span></a></p></td><td  style='padding:7.5pt 7.5pt 7.5pt 3.75pt;'><p  style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/invest/' target='_blank'><span style='color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo1.png' alt='HighScope 50th logo' style='width:156.6pt;height:77.4pt;'></span></a><span style='font-size: 9pt;'><u></u><u></u></span></p></td></tr></tbody></table></td></tr><tr><td><div style='border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;'></div></td></tr><tr><td><p style='margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;'>Hi Admin,</p><p style='margin-top:0;margin-bottom: 7.5px;color: rgb(40, 40, 40);font-size: 12pt;'>Customer ".$name." wishes to order for ".$location." location, please find below details.</p><p style='margin-top:0;margin-bottom: 7.5px;color: rgb(40, 40, 40);font-size: 12pt;'></p><p style='font-size: 14px; line-height: 1.3; '><br><strong>Customer Details</strong></p><p style='font-size: 14px; line-height: 1.3; '><br>Email : ".$mail."</p><p style='font-size: 14px; line-height: 1.3; '><br>Contact Number : ".$phone."</p><p style='font-size: 14px; line-height: 1.3; '><br>Address : ".$address."</p><p style='font-size: 14px; line-height: 1.3; '><br><strong>Product Details</strong></p>    <p style='font-size: 14px; line-height: 1.3; '><p style='font-size: 14px; line-height: 1.3; '><br>Email : ".$mail."</p> </td></tr><tr><td style='text-align: center;'></td></tr><tr><td><div style='border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;'></div></td></tr><tr><td style='text-align: center;'><ul style='padding-left:0;list-style:none;><li style='display: inline-block;'><a href='https://twitter.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png'  alt='Twitter' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FHighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/facebook.png'  alt='Facebook' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.instagram.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/instagram.png'  alt='Instagram' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.linkedin.com/company/highscopeus' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/linkedin.png'  alt='Linkedin' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://twitter.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png'  alt='Twitter' style='width:18pt;height:18pt;'></a></li></ul></td></tr><tr><td style='background-color:#CCCCCC; padding:7.5pt;'><div><p  style='text-align:center;margin-top:0;margin-bottom:0;'><span style='font-size: 9pt;'>HighScope Educational Research Foundation<u></u><u></u></span></p></div><div><p style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/' target='_blank' style='font-size: 9pt;'>HighScope.org</a></p></div></td></tr></tbody></table></body></html>";
    $headers1 = "MIME-Version: 1.0" . "\r\n";
    $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers1 .= 'From: hscst@highscope.org'. "\r\n";
    wp_mail($admin_email, $subject, $message, $headers1);
    echo json_encode(array('status' => 1));
    die();   
}
add_action("wp_ajax_send_mail_internation_ship", "send_mail_internation_ship");
add_action("wp_ajax_nopriv_send_mail_internation_ship", "send_mail_internation_ship");

/*********** function for virtual order notes ********************/
function virtual_order_notes($data){
    echo '<p class="form-row notes" data-priority=""><label for="order_comments" class="">Order notes&nbsp;<span class="optional">(optional)</span></label><span class="woocommerce-input-wrapper"><textarea name="virtual_box" class="input-text " id="virtual_box" placeholder="Notes about your order, e.g. special notes for delivery." rows="2" cols="5"></textarea></span></p><br>';

    if(!get_user_meta(get_current_user_id(), 'tex_exempt', true )){
    	echo '<p class="form-row notes" data-priority=""><span class="woocommerce-input-wrapper">
    	<input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox tax_exempts" name="tax_exempts" value="1">If you are a tax-exempt customer, please select the checkbox to fill in the information.
    	</span></p><br>';
    }	
    ?>

    <div class="tax-expept-process hide">
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
            <label for="account_first_name">Exemption Type&nbsp;<span class="required">*</span></label>
            <select class="woocommerce-Input woocommerce-Input--text input-text" name="exempt_type" required="">
                <option value="">Non-Exempt</option>
                <option value="wholesale">Wholesale / Resale</option>
                <option value="government">Government</option>
                <option value="other">Other</option>
            </select>    
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
            <label for="account_last_name">Exempt States (Can choose multiple states)&nbsp;<span class="required">*</span></label>
            <select name="exempt_states[]" class="multipels woocommerce-Input woocommerce-Input--text input-text" multiple required="">
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
            </select> 
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
            <label for="account_first_name">Tax Certificate&nbsp;<span class="required">*</span></label>
            <input type="file" id="files" name="tax_certificate" accept="application/pdf" required=""/>
             <input id="pdf_base649" name="pdf_base649" type="hidden" value="" />
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
            <label for="account_first_name">Certificate	Expiration&nbsp;<span class="required">*</span></label>
            <input type="date" class="woocommerce-Input woocommerce-Input--text input-text" name="certificate_expire" value="<?php echo $certificate_expire; ?>" />   
        </p>
        <div class="clear"></div>
        <p>
            <input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>"><br><br>
        <!-- <button type="button" class="woocommerce-Button button save_tax_jars" name="save_tax_jar" value="Save">Save</button><br><br> -->
        </p>
	</div>	

<?php }
add_action("woocommerce_review_order_before_payment", "virtual_order_notes", 10, 1);



/*********** function for add taxjar option on my account page **************/
add_filter ( 'woocommerce_account_menu_items', 'highscope_log_history_link', 40 );
function highscope_log_history_link( $menu_links ){
    
    $menu_links = array_slice( $menu_links, 0, 5, true ) 
    + array( 'tax-jar' => 'Sales Tax Exemption' )
    + array_slice( $menu_links, 5, NULL, true );
    
    return $menu_links;

}

/*********** function for add taxjar option on my account page **************/
add_action( 'init', 'highscope_add_endpoint' );
function highscope_add_endpoint() {
    add_rewrite_endpoint( 'tax-jar', EP_PAGES );

}

/*********** function for  taxjar front end part **************/
add_action( 'woocommerce_account_tax-jar_endpoint', 'highscope_my_account_endpoint_content' );
function highscope_my_account_endpoint_content() { 
    global $wpdb;
    $tablename = $wpdb->prefix.'taxjar_entries';
    if(!empty($_POST['save_tax_jar'])){
        $user_id = $_POST['user_id'];
        $exempt_type = $_POST['exempt_type'];
        $exempt_states = implode(',', $_POST['exempt_states']);
        $certificate_expire = $_POST['certificate_expire'];
        $tax_certificate = '';
        if (!empty($_POST['tax_certificate'] )) {
            $tax_certificate = save_pdf_canvas($user_id, $_POST['pdf_base64'],'certificate_'.time());
        }
        
        $wpdb->get_results("
            SELECT id FROM " . $tablename." 
            WHERE 
            user_id = '" . $user_id . "'"); 
        if($wpdb->num_rows){
            $sql = "UPDATE $tablename SET `exempt_type`='%s', `exempt_states`='%s', `tax_certificate`='%s', `certificate_expire`='%s', `status`='%s' WHERE user_id=%s";
            $query = sprintf($sql,
                $_POST['exempt_type'],
                $exempt_states,
                $tax_certificate,
                $certificate_expire,
                3,
                $user_id
            );
            $wpdb->query($query);
        }else{
        	$user = get_user_by( 'id', $user_id);
        	$uemail = $user->user_email;
        	$uname = $user->first_name . ' ' . $user->last_name;
            $query = "INSERT INTO $tablename (user_id, user_name, user_email, exempt_type, exempt_states, tax_certificate, certificate_expire, status) VALUES ('$user_id','$uname','$uemail','$exempt_type','$exempt_states','$tax_certificate','$certificate_expire','3')";
                $wpdb->query($query);
        }

        update_user_meta(get_current_user_id(), 'tex_exempt', 1);    
        send_email_text_exemptions($uname, $uemail, $exempt_type, $exempt_states, $tax_certificate, $certificate_expire);    
    }    

    $ids = get_current_user_id();
    $data = $wpdb->get_results("SELECT * FROM $tablename WHERE user_id = '$ids'", ARRAY_A);
    $status = $exempt_type = $exempt_states = $tax_certificate = '';   
    if(!empty($data)){
        $status = ($data[0]['status'])?$data[0]['status']:'';
        $exempt_type = ($data[0]['exempt_type'])?$data[0]['exempt_type']:'';
        $exempt_states = ($data[0]['exempt_states'])?$data[0]['exempt_states']:'';
        $tax_certificate = ($data[0]['tax_certificate'])?$data[0]['tax_certificate']:'';
        $status = ($data[0]['status'])?$data[0]['status']:'';
        $state_array = explode(',', $exempt_states);
        $certificate_expire = ($data[0]['certificate_expire'])?$data[0]['certificate_expire']:'';
    }
    ?>
    <div class="taxjar-myacount">
        <h2>Sales Tax Exemption</h2>
        <?php
            $expired = '';
            if($certificate_expire < date('Y-m-d')){
                $expired = 1;
            }
            if($expired == ''){
                if($status == 3){ ?>
                <div class="woocommerce-info">Status: Verification Pending!</div>
                <?php }else if($status == 1){ ?>
                <div class="woocommerce-info">Status: Verified! Eligible for Tax Exemption</div>
                <?php }else if($status == 2){ ?>
                <div class="woocommerce-info">Status: Verification Declined! Please resubmit Tax Exemption information.</div>
                <?php }
            }else {
                if(!empty($certificate_expire)){ ?>
                    <div class="woocommerce-info">Status: Certificate Expired! Please resubmit Tax Exemption information.</div>
            <?php } }?>

        <form class="woocommerce-taxjar-form" action="" method="post">
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                <label for="account_first_name">Exemption Type&nbsp;<span class="required">*</span></label>
                <select class="woocommerce-Input woocommerce-Input--text input-text" name="exempt_type" required="">
                    <option value="" <?php echo ($exempt_type == '')?'selected':''; ?> >Non-Exempt</option>
                    <option value="wholesale" <?php echo ($exempt_type == 'wholesale')?'selected':''; ?> >Wholesale / Resale</option>
                    <option value="government" <?php echo ($exempt_type == 'government')?'selected':''; ?> >Government</option>
                    <option value="other" <?php echo ($exempt_type == 'other')?'selected':''; ?>>Other</option>
                </select>    
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
                <label for="account_last_name">Exempt States (Can choose multiple states)&nbsp;<span class="required">*</span></label>
                <select name="exempt_states[]" class="multipels woocommerce-Input woocommerce-Input--text input-text" multiple required="">
                    <option value="AL" <?php echo (in_array('AL', $state_array))?'selected':''; ?> >Alabama</option>
                    <option value="AK" <?php echo (in_array('AK', $state_array))?'selected':''; ?> >Alaska</option>
                    <option value="AZ" <?php echo (in_array('AZ', $state_array))?'selected':''; ?> >Arizona</option>
                    <option value="AR" <?php echo (in_array('AR', $state_array))?'selected':''; ?> >Arkansas</option>
                    <option value="CA" <?php echo (in_array('CA', $state_array))?'selected':''; ?> >California</option>
                    <option value="CO" <?php echo (in_array('CO', $state_array))?'selected':''; ?> >Colorado</option>
                    <option value="CT" <?php echo (in_array('CT', $state_array))?'selected':''; ?> >Connecticut</option>
                    <option value="DE" <?php echo (in_array('DE', $state_array))?'selected':''; ?> >Delaware</option>
                    <option value="FL" <?php echo (in_array('FL', $state_array))?'selected':''; ?> >Florida</option>
                    <option value="GA" <?php echo (in_array('GA', $state_array))?'selected':''; ?> >Georgia</option>
                    <option value="HI" <?php echo (in_array('HI', $state_array))?'selected':''; ?> >Hawaii</option>
                    <option value="ID" <?php echo (in_array('ID', $state_array))?'selected':''; ?> >Idaho</option>
                    <option value="IL" <?php echo (in_array('IL', $state_array))?'selected':''; ?> >Illinois</option>
                    <option value="IN" <?php echo (in_array('IN', $state_array))?'selected':''; ?> >Indiana</option>
                    <option value="IA" <?php echo (in_array('IA', $state_array))?'selected':''; ?> >Iowa</option>
                    <option value="KS" <?php echo (in_array('KS', $state_array))?'selected':''; ?> >Kansas</option>
                    <option value="KY" <?php echo (in_array('KY', $state_array))?'selected':''; ?> >Kentucky</option>
                    <option value="LA" <?php echo (in_array('LA', $state_array))?'selected':''; ?> >Louisiana</option>
                    <option value="ME" <?php echo (in_array('ME', $state_array))?'selected':''; ?> >Maine</option>
                    <option value="MD" <?php echo (in_array('MD', $state_array))?'selected':''; ?> >Maryland</option>
                    <option value="MA" <?php echo (in_array('MA', $state_array))?'selected':''; ?> >Massachusetts</option>
                    <option value="MI" <?php echo (in_array('MI', $state_array))?'selected':''; ?> >Michigan</option>
                    <option value="MN" <?php echo (in_array('MN', $state_array))?'selected':''; ?> >Minnesota</option>
                    <option value="MS" <?php echo (in_array('MS', $state_array))?'selected':''; ?> >Mississippi</option>
                    <option value="MO" <?php echo (in_array('MO', $state_array))?'selected':''; ?> >Missouri</option>
                    <option value="MT" <?php echo (in_array('MT', $state_array))?'selected':''; ?> >Montana</option>
                    <option value="NE" <?php echo (in_array('NE', $state_array))?'selected':''; ?> >Nebraska</option>
                    <option value="NV" <?php echo (in_array('NV', $state_array))?'selected':''; ?> >Nevada</option>
                    <option value="NH" <?php echo (in_array('NH', $state_array))?'selected':''; ?> >New Hampshire</option>
                    <option value="NJ" <?php echo (in_array('NJ', $state_array))?'selected':''; ?> >New Jersey</option>
                    <option value="NM" <?php echo (in_array('NM', $state_array))?'selected':''; ?> >New Mexico</option>
                    <option value="NY" <?php echo (in_array('NY', $state_array))?'selected':''; ?> >New York</option>
                    <option value="NC" <?php echo (in_array('NC', $state_array))?'selected':''; ?> >North Carolina</option>
                    <option value="ND" <?php echo (in_array('ND', $state_array))?'selected':''; ?> >North Dakota</option>
                    <option value="OH" <?php echo (in_array('OH', $state_array))?'selected':''; ?> >Ohio</option>
                    <option value="OK" <?php echo (in_array('OK', $state_array))?'selected':''; ?> >Oklahoma</option>
                    <option value="OR" <?php echo (in_array('OR', $state_array))?'selected':''; ?> >Oregon</option>
                    <option value="PA" <?php echo (in_array('PA', $state_array))?'selected':''; ?> >Pennsylvania</option>
                    <option value="RI" <?php echo (in_array('RI', $state_array))?'selected':''; ?> >Rhode Island</option>
                    <option value="SC" <?php echo (in_array('SC', $state_array))?'selected':''; ?> >South Carolina</option>
                    <option value="SD" <?php echo (in_array('SD', $state_array))?'selected':''; ?> >South Dakota</option>
                    <option value="TN" <?php echo (in_array('TN', $state_array))?'selected':''; ?> >Tennessee</option>
                    <option value="TX" <?php echo (in_array('TX', $state_array))?'selected':''; ?> >Texas</option>
                    <option value="UT" <?php echo (in_array('UT', $state_array))?'selected':''; ?> >Utah</option>
                    <option value="VT" <?php echo (in_array('VT', $state_array))?'selected':''; ?> >Vermont</option>
                    <option value="VA" <?php echo (in_array('VA', $state_array))?'selected':''; ?> >Virginia</option>
                    <option value="WA" <?php echo (in_array('WA', $state_array))?'selected':''; ?> >Washington</option>
                    <option value="WV" <?php echo (in_array('WV', $state_array))?'selected':''; ?> >West Virginia</option>
                    <option value="WI" <?php echo (in_array('WI', $state_array))?'selected':''; ?> >Wisconsin</option>
                    <option value="WY" <?php echo (in_array('WY', $state_array))?'selected':''; ?> >Wyoming</option>
                </select> 
            </p>
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                <label for="account_first_name">Tax Certificate&nbsp;<span class="required">*</span></label>
                <input type="file" id="files" name="tax_certificate" accept="application/pdf" required=""/>
                 <input id="pdf_base64" name="pdf_base64" type="hidden" />
                    <?php if(!empty($tax_certificate)) { ?>
                    <br><a href="<?php echo $tax_certificate; ?>" target="_blank">Certificate</a>
                    <?php } ?>
                    </p>
                 <script type="text/javascript">
                    document.getElementById('files').addEventListener('change', handleFileSelect, false);
                    function handleFileSelect(evt) {
                        var f = evt.target.files[0]; // FileList object
                        if(f.type == 'application/pdf'){
                            var reader = new FileReader();
                            reader.onload = (function(theFile) {
                            return function(e) {
                                var binaryData = e.target.result;
                                var base64String = window.btoa(binaryData);
                                document.getElementById('pdf_base64').value = base64String;
                            };
                            })(f);
                            reader.readAsBinaryString(f);
                        }else{
                            alert('Please select pdf file');
                            document.getElementById('files').value=''
                        }   
                    }
            </script>
            <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                <label for="account_first_name">Certificate	Expiration&nbsp;<span class="required">*</span></label>
                <input type="date" class="woocommerce-Input woocommerce-Input--text input-text" name="certificate_expire" value="<?php echo $certificate_expire; ?>" required=""/>   
            </p>
            <div class="clear"></div>
            <p>
                <input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>"><br><br>
                <button type="submit" class="woocommerce-Button button" name="save_tax_jar" value="Save" <?php echo ($expired != 1 && ($status == 1 || $status == 3))?'disabled':''; ?> >Save</button><br><br>
            </p>
        </form>
    </div>    
<?php }


/*********** function for  add new menu **************/
add_action('admin_menu', 'custom_menu');
function custom_menu() { 
 	add_menu_page( 'Tax Exemption', 'Tax Exemption', 'edit_posts', 'tax_exempt', 'tax_exempt_callback_function', 'dashicons-media-spreadsheet' 
    );
} 

/*********** function for new menuc all back **************/
function tax_exempt_callback_function(){
	global $wpdb;
    $tablename = $wpdb->prefix.'taxjar_entries'; 
	$data = $wpdb->get_results("SELECT * FROM $tablename", ARRAY_A);

	?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#tax-exempts').DataTable();

			jQuery(document).on('click', '.tax_action', function(){
				var _action = jQuery(this).attr('next_step');
				var _ids = jQuery(this).attr('ids');
				
				swal({
					title: "Are you sure?",
					text: "Do you want to change status!",
					type: "warning",
					buttons: ["Cancel", "Continue"],
				}).then((willDelete) => {
					var data = {
						'action': 'change_status_text_exempt',
						'id': _ids,
						'status': _action,
					};
	               if(willDelete){
						jQuery.ajax({
							url: ajax_url,
							type: 'post',
							data: data,
							dataType: 'json',
							success: function(data){
								if(data.status == 1){
									location.reload();
								}
							}
						});
	               }
	            });

			});
		} );
	</script>
	<div class="wrap">
		<h2 class="tax-exempts">Tax Exempt Users</h2>
	<table id="tax-exempts" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Exempt Type</th>
                <th>Exempt States</th>
                <th>Tax Certificate</th>
                <th>Certificate Expiration</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        	<?php
        	foreach($data as $value){ ?>
            <tr>
            	<?php $user = get_user_by( 'id', $value['user_id']);?>
                <td><?php echo $value['user_name'];?></td>
                <td><?php echo $value['user_email'];?></td>
                <td><?php echo $value['exempt_type'];?></td>
                <td><?php echo $value['exempt_states'];?></td>
                <td><?php echo '<a target="_blank" href="'.$value['tax_certificate'].'">Certificate</a>';?></td>
                <td><?php echo  date("d-m-Y", strtotime($value['certificate_expire']));?></td>
                <td><?php echo get_status_str($value['status']);?></td>
                <td>
                	<?php 
                	if($value['status'] == 3){ ?>
	                	<button class="btn btn-primary tax_action" ids="<?php echo $value['id']; ?>" next_step="1" >Appove</button>
	                	<button class="btn btn-danger rejected tax_action" ids="<?php echo $value['id']; ?>" next_step="2" >Reject</button>
                <?php } 
                    if($value['status'] == 2){ ?>
                        <button class="btn btn-primary tax_action" ids="<?php echo $value['id']; ?>" next_step="1" >Appove</button>
                        
                <?php } 
                    if($value['status'] == 1){ ?>
                        <button class="btn btn-danger rejected tax_action" ids="<?php echo $value['id']; ?>" next_step="2" >Reject</button>
                <?php } ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Exempt Type</th>
                <th>Exempt States</th>
                <th>Tax Certificate</th>
                <th>Certificate Expiration</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>
 </div>   
<?php }


/*********** function for get status of tax exempt **************/
function get_status_str($str){
	$return = '';
	switch ($str) {
		case "1":
		$return = "Approved";
		break;
		case "2":
		$return = "Rejected";
		break;
		case "3":
		$return = "Pending";
		break;
	}
	return $return;
}


/*********** function for change status of tax exempt **************/
function change_status_text_exempt(){
	global $wpdb;
	$id = $_POST['id'];
	$status = $_POST['status'];
    $tablename = $wpdb->prefix.'taxjar_entries';
	$sql = "UPDATE $tablename SET `status`='%s' WHERE id=%s";
		$query = sprintf($sql,
		$status,
		$id
	);
	$wpdb->query($query);
	echo json_encode(array('status' => 1));
    die(); 
}
add_action("wp_ajax_change_status_text_exempt", "change_status_text_exempt");


/*********** function for change thankyou message **************/
add_filter( 'woocommerce_thankyou_order_received_text', 'd4tw_custom_ty_msg', 10, 2);
function d4tw_custom_ty_msg ($thank_you_msg, $order) {
    $order_id = $order->get_id();
    if(get_post_meta( $order_id, 'tex_exempt_order', true)){
        $thank_you_msg =  'Thank you. Your order has been received. <br>You have successfully submitted the information regarding tax exemption. Our system will verify your tax information, and update the verification status on your profile details. If you wish to expedite this process, please contact High Scope Customer support at 800.587.5639.';
        echo '<br>';
    }else{
        $thank_you_msg =  'Thank you. Your order has been received.';
        echo '<br>';
    } 
    return $thank_you_msg;
}


/*********** function for send text emails **************/
function send_email_text_exemptions($name, $email, $exempt_type, $exempt_states, $tax_certificate, $certificate_expire){
    $admin_email = get_option('admin_email');
    $subject = 'Tax Exemption';
    
   $message = "<!doctype html><html lang='en'><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'><meta content='width=device-width' name='viewport'><title>Email</title></head><body style='margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #fff;'><table   style='width:480pt; padding: 11.25pt 0' ><tbody><tr><td  style='padding:0;'><table  ><tbody><tr><td  style='padding:7.5pt 3.75pt 7.5pt 7.5pt;'><p  style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/' target='_blank'><span style='color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;'><img src='https://highscopedev.wpengine.com/wp-content/uploads/2021/07/8124a5cb070288947c89e0e7_780x206.png'  alt='HighScope Logo' style='width:292.19pt;height:77.4pt;'></span></a></p></td><td  style='padding:7.5pt 7.5pt 7.5pt 3.75pt;'><p  style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/invest/' target='_blank'><span style='color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo1.png' alt='HighScope 50th logo' style='width:156.6pt;height:77.4pt;'></span></a><span style='font-size: 9pt;'><u></u><u></u></span></p></td></tr></tbody></table></td></tr><tr><td><div style='border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;'></div></td></tr><tr><td><p style='margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;'>Hi Admin,</p><p style='margin-top:0;margin-bottom: 7.5px;color: rgb(40, 40, 40);font-size: 12pt;'>Customer ".$name." has sent some information regarding tax exemption.</p><p style='margin-top:0;margin-bottom: 7.5px;color: rgb(40, 40, 40);font-size: 12pt;'></p><p>Please refer to the attached information, and update it in TaxJar. You can also see all the information in Tax Exemption menu.</p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br><strong>Customer Details</strong></p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br>Name : ".$name."</p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br>Email : ".$email."</p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br>Exemption Type : ".$exempt_type."</p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br>Exempt States : ".$exempt_states."</p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br>Tax Certificate : <a href=".$tax_certificate." target='_blank'>Certificate</a></p><p style='font-size: 14px; line-height: 1.3;   margin: 0;'><br>Certificate Expiration : ".date("d-m-Y", strtotime($certificate_expire))."</p>  </td></tr><tr><td style='text-align: center;'></td></tr><tr><td><div style='border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;'></div></td></tr><tr><td style='text-align: center;'><ul style='padding-left:0;list-style:none;><li style='display: inline-block;'><a href='https://twitter.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png'  alt='Twitter' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FHighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/facebook.png'  alt='Facebook' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.instagram.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/instagram.png'  alt='Instagram' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.linkedin.com/company/highscopeus' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/linkedin.png'  alt='Linkedin' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://twitter.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png'  alt='Twitter' style='width:18pt;height:18pt;'></a></li></ul></td></tr><tr><td style='background-color:#CCCCCC; padding:7.5pt;'><div><p  style='text-align:center;margin-top:0;margin-bottom:0;'><span style='font-size: 9pt;'>HighScope Educational Research Foundation<u></u><u></u></span></p></div><div><p style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/' target='_blank' style='font-size: 9pt;'>HighScope.org</a></p></div></td></tr></tbody></table></body></html>";
    $headers1 = "MIME-Version: 1.0" . "\r\n";
    $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers1 .= 'From: hscst@highscope.org'. "\r\n";
    wp_mail($admin_email, $subject, $message, $headers1);
}

/*********** function for send registration email **************/
function send_email_user_registration($first_name, $last_name, $mailid){
    $admin_email = get_option('admin_email');
    $subject = 'Thanks for sign up.';
    $message = "<!doctype html><html lang='en'><head><meta content='text/html; charset=utf-8' http-equiv='Content-Type'><meta content='width=device-width' name='viewport'><title>Email</title></head><body style='margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #fff;'><table   style='width:480pt; padding: 11.25pt 0' ><tbody><tr><td  style='padding:0;'><table  ><tbody><tr><td  style='padding:7.5pt 3.75pt 7.5pt 7.5pt;'><p  style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/' target='_blank'><span style='color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo.png'  alt='HighScope Logo' style='width:292.19pt;height:77.4pt;'></span></a></p></td><td  style='padding:7.5pt 7.5pt 7.5pt 3.75pt;'><p  style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/invest/' target='_blank'><span style='color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo1.png' alt='HighScope 50th logo' style='width:156.6pt;height:77.4pt;'></span></a><span style='font-size: 9pt;'><u></u><u></u></span></p></td></tr></tbody></table></td></tr><tr><td><div style='border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;'></div></td></tr><tr><td><p style='margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;'>Hi ".$first_name." ".$last_name.",</p><p style='margin-top:0;margin-bottom: 7.5px;color: rgb(40, 40, 40);font-size: 12pt;'>Thanks for signing up to HighScope.org!</p><p><a href='https://highscopedev.wpengine.com/shop/' target='_blank' style='color: white;font-size: 13.5pt;background-color: rgb(1, 33, 105);text-decoration: none;padding: 12pt;border: 1pt solid rgb(1, 33, 105);font-weight: bold;display: inline-block;margin: 5pt auto 0pt;border-radius: 6px;'>SEE WHAT'S NEW</a></p><p style='margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;'>We hope you enjoy the new look of the HighScope Online Store!</p></td></tr><tr><td style='text-align: center;'></td></tr><tr><td><div style='border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;'></div></td></tr><tr><td style='text-align: center;'><ul style='padding-left:0;list-style:none;'><li style='display: inline-block;'><a href='https://twitter.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png'  alt='Twitter' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FHighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/facebook.png'  alt='Facebook' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.instagram.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/instagram.png'  alt='Instagram' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://www.linkedin.com/company/highscopeus' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/linkedin.png'  alt='Linkedin' style='width:18pt;height:18pt;'></a></li><li style='display: inline-block;'><a href='https://twitter.com/HighScopeUS' target='_blank' style='color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;'><img src='https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png'  alt='Twitter' style='width:18pt;height:18pt;'></a></li></ul></td></tr><tr><td style='background-color:#CCCCCC; padding:7.5pt;'><div><p  style='text-align:center;margin-top:0;margin-bottom:0;'><span style='font-size: 9pt;'>HighScope Educational Research Foundation<u></u><u></u></span></p></div><div><p style='text-align:center;margin-top:0;margin-bottom:0;'><a href='https://highscope.org/' target='_blank' style='font-size: 9pt;'>HighScope.org</a></p></div></td></tr></tbody></table></body></html>";
    $headers1 = "MIME-Version: 1.0" . "\r\n";
    $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers1 .= 'From: hscst@highscope.org'. "\r\n";

    wp_mail( $mailid, $subject, $message, $headers1 ); 
}

/*********** function for return html email **************/
function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );


/*********** function for remove additinal information **************/
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}

/*********** function for add new menu for user import **************/
add_action('admin_menu', 'import_users_data');
function import_users_data() { 
    add_menu_page( 'Import Users', 'Import Users', 'edit_posts', 'import_users', 'import_users_callback_function', 'dashicons-media-spreadsheet' 
    );
}

/*********** function for call back of users **************/
function import_users_callback_function(){
    $error = 0; 
    if(!empty($_FILES['import_users'])){
        if($_FILES['import_users']['type'] == 'text/csv'){
           $csvFile = fopen($_FILES['import_users']['tmp_name'], 'r');
            fgetcsv($csvFile);
            $counter = 0;
            while(($data = fgetcsv($csvFile)) !== FALSE){                
                $names = explode(" ",$data[2]);
                $title = $data[1];
                $first_name = $names[0];
                $last_name = $names[1];
                $company = $data[0];
                $mailid  = $data[3];
                $password = $data[3];
                $username = strstr($mailid, '@', true);
                $address = $data[6];
                $city = $data[7];
                $country = $data[10];
                $state = $data[9];
                $zipcode = $data[8];
                $phone = $data[4];
                $fax = $data[5];

                $userdata = array(
                    'user_login'    =>   $username,
                    'user_email'    =>   $mailid,
                    'user_pass'     =>   $password,
                    'user_registered' => date('Y-m-d H:i:s'),
                );
                $user_ids = wp_insert_user($userdata);

                update_user_meta( $user_ids, 'title', $title);
                update_user_meta( $user_ids, 'first_name', $first_name);
                update_user_meta( $user_ids, 'last_name', $last_name);
                update_user_meta( $user_ids, 'billing_company', $company);
                update_user_meta( $user_ids, 'billing_address_1', $address);
                update_user_meta( $user_ids, 'billing_city', $city);
                update_user_meta( $user_ids, 'billing_postcode', $zipcode);
                update_user_meta( $user_ids, 'billing_state', $state);
                update_user_meta( $user_ids, 'billing_phone', $phone);
                update_user_meta( $user_ids, 'fax', $fax);
                $counter++;
             }    
        }else{
            $error = 1;
        }     
    } ?>
    <div class="wrap">
        <h1>Import Users</h1>
        <p>Here you can import users with csv files.</p>
        <form method="post" action="" enctype="multipart/form-data">
            <table class="form-table">
                <tbody>
                <tr valign="top">
                    <th scope="row">Import</th>
                    <td><input type="file" name="import_users" required=""/></td>
                </tr>
                </tbody>
            </table>
            <?php
                if($error == 1){
                    echo '<p style="color:red">Please select .csv file</p>';
                }else if($counter > 0){ 
                	echo '<p style="color:green">'.$counter.' users imported successfully.</p>';
 				} ?> 	
            <p class="submit"><input type="submit" name="submit" class="button button-primary" value="Import"></p>
        </form>
    </div>
    <style type="text/css">.error{display: none;}.notice.notice-warning.fade {display: none;}</style>
<?php } 


/*********** function for add shop option in breadcrums **************/
add_filter( 'woocommerce_get_breadcrumb', 'bbloomer_single_product_edit_cat_breadcrumbs', 9999, 2 );
function bbloomer_single_product_edit_cat_breadcrumbs($crumbs, $breadcrumb) {
    array_unshift($crumbs, array('Shop', '/store/'));
    $temp = $crumbs[0];
    $crumbs[0] = $crumbs[1];
    $crumbs[1] = $temp;
    $counter = 0;
    foreach($crumbs as $value){
        if($counter > 1 && $counter < count($crumbs)-1){
            $url = get_cates_id_by_name($crumbs[$counter][0]);
            $crumbs[$counter][1] = '/store-category/?c='.$url;
        }
        $counter++;
    }
    return $crumbs;
}

/*********** function for get category id by name **************/
function get_cates_id_by_name($name){
    global $wpdb;
    $results = $wpdb->get_results("SELECT {$wpdb->prefix}terms.term_id FROM {$wpdb->prefix}terms 
        INNER JOIN {$wpdb->prefix}termmeta ON {$wpdb->prefix}terms.term_id={$wpdb->prefix}termmeta.term_id
        WHERE `name` = '$name'", OBJECT );
    return $results[0]->term_id;
}

/*********** function for get bredcrums for category **************/
function get_breadcrums_category($id){
    $cates_array = array();
    $parent_one = get_ancestors($id, 'product_cat');
    if(isset($parent_one)){
        $cates_array['second'] = $parent_one[0]; 
        $parent_two = get_ancestors($parent_one[0], 'product_cat');
    }
    if(!empty($parent_two)){
        $cates_array['first'] = $parent_two[0]; 
    }
    $cates_array['childs'] = $id;
    return $cates_array;
}

add_filter( 'woocommerce_breadcrumb_defaults', 'my_change_breadcrumb_delimiter' );
function my_change_breadcrumb_delimiter( $defaults ) {
    $defaults['delimiter'] = ' <span class="addSpace">|</span> ';
    return $defaults;
}

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
function tutsplus_excerpt_in_product_archives() { ?>
     
    <h6 class="woocommerce-loop-product__discription relared">
     	<?php 
         $content = strip_tags(get_the_content());
         $content = apply_filters('the_content', $content);
                echo substr_replace(strip_tags($content), "...", 150); ?>
    </h6>
	<div class="product-code">Code: <?php echo (get_post_meta( get_the_ID(), '_sku', true ))?get_post_meta( get_the_ID(), '_sku', true ):'NA'; ?></div>
	<span class="price">
		<span class="woocommerce-Price-amount amount">
			<bdi>
			<?php 
			global $post;
			$product = new WC_Product($post->ID); 
			echo  strip_tags(wc_price($product->get_price_including_tax(1,$product->get_price())));
			?>
                <span class="p-quantity">/ Each</span>
				<span class="price-tag">Retail price</span>
			</bdi>
		</span>
	</span>
<div class="gridlist-buttonwrap"></div>         
<?php }
add_action( 'woocommerce_after_shop_loop_item_title', 'tutsplus_excerpt_in_product_archives', 40 );


function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}
add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 );