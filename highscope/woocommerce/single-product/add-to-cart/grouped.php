<?php
    /**
     * Grouped product add to cart
     *
     * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/grouped.php.
     *
     * HOWEVER, on occasion WooCommerce will need to update template files and you
     * (the theme developer) will need to copy the new files to your theme to
     * maintain compatibility. We try to do this as little as possible, but it does
     * happen. When this occurs the version of the template file will be bumped and
     * the readme will list any important changes.
     *
     * @see         https://docs.woocommerce.com/document/template-structure/
     * @package     WooCommerce/Templates
     * @version     3.4.0
     */

    defined( 'ABSPATH' ) || exit;

    global $product, $post;

    do_action( 'woocommerce_before_add_to_cart_form' ); ?>

    <form class="cart grouped_form" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>


        <?php
          $quantites_required      = false;
          $previous_post           = $post;
          $grouped_product_columns = apply_filters( 'woocommerce_grouped_product_columns', array(
            'quantity',
            'label',
            'price',
          ), $product );
          $options = '<option value="">Select</option>';
          foreach ( $grouped_products as $grouped_product_child ) {
            $post_object        = get_post( $grouped_product_child->get_id() );
            $quantites_required = $quantites_required || ( $grouped_product_child->is_purchasable() && ! $grouped_product_child->has_options() );
            $post               = $post_object; // WPCS: override ok.
            setup_postdata( $post );

            foreach ( $grouped_product_columns as $column_id ) {
              do_action( 'woocommerce_grouped_product_list_before_' . $column_id, $grouped_product_child );
              if ($column_id === 'quantity'){
                do_action( 'woocommerce_before_add_to_cart_quantity' );
                $options .= '<option value="quantity[' . $grouped_product_child->get_id() . ']" urls="'.$grouped_product_child->product_url.'" types="'.$grouped_product_child->get_type().'" ispn="'.$grouped_product_child->get_sku().'" descrption="'.$grouped_product_child->get_description().'" short_descrption="'.$grouped_product_child->get_short_description().'">' . $grouped_product_child->get_name() .$grouped_product_child->get_price_html(). '</option>';
                do_action( 'woocommerce_after_add_to_cart_quantity' );
              }
              

              do_action( 'woocommerce_grouped_product_list_after_' . $column_id, $grouped_product_child );
            }
           // echo '</div>';

          }
          ?>
          <div class="row">
            <div class="col-6 mb-3">
              <?php
              echo '<select class="multi-prod form-control">'.$options.'</select>';
              echo '<input type="hidden" class="input-text qty text" name="369499" value="1">';
              $post = $previous_post; // WPCS: override ok.
              setup_postdata( $post );
              ?>
            </div>
          </div>

      <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" />

      <?php if ( $quantites_required ) : ?>

        <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

        <br><button type="submit" class="single_add_to_cart_button btn btn-success">ADD TO CART</button>

        <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

      <?php endif; ?>
    </form>

    <?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

 <script>
   
   jQuery('.single_add_to_cart_button').click(function(e){
      var _is_extrernal = jQuery('.multi-prod').find('option:selected').attr('types');
      if(_is_extrernal == 'external'){
        e.preventDefault();
        var _url = jQuery('.multi-prod').find('option:selected').attr('urls');
        window.location.href = _url;
      }
      
   });
    
   jQuery('.multi-prod').change(function(e){
      var  $this = jQuery(this);
      var _ispn = jQuery('option:selected', this).attr('ispn');
      var _shrt_desc = jQuery('option:selected', this).attr('short_descrption');
      var _descrption = jQuery('option:selected', this).attr('descrption');
      jQuery('.qty').attr('name', $this.val());
      jQuery('.dfsd').find('.sku_wrapper').find('.sku').text(_ispn);
      jQuery('.product-short-descp').html(_shrt_desc);
      jQuery('.long-description').html(_descrption);
  });


 </script>   