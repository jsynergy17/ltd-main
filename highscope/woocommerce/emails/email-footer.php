<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top">
						<!-- Footer -->
						<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
							<tr>
								<td valign="top">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td colspan="2" valign="middle" id="credit">
												<table style="width:480pt; padding:0 0 11.25pt" ><tbody><tr><td style="text-align: center;"><ul style="list-style:none;text-align: center;"><li style="display: inline-block;"><a href="https://twitter.com/HighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png"  alt="Twitter" style="width:18pt;height:18pt;"></a></li><li style="display: inline-block;"><a href="https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FHighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/facebook.png"  alt="Facebook" style="width:18pt;height:18pt;"></a></li><li style="display: inline-block;"><a href="https://www.instagram.com/HighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/instagram.png"  alt="Instagram" style="width:18pt;height:18pt;"></a></li><li style="display: inline-block;"><a href="https://www.linkedin.com/company/highscopeus" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/linkedin.png"  alt="Linkedin" style="width:18pt;height:18pt;"></a></li><li style="display: inline-block;"><a href="https://twitter.com/HighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/twitter.png"  alt="Twitter" style="width:18pt;height:18pt;"></a></li></ul></td></tr><tr><td style="background-color:#CCCCCC; padding:7.5pt;"><div><p  style="text-align:center;margin-top:0;margin-bottom:0;"><span style="font-size: 9pt;">HighScope Educational Research Foundation<u></u><u></u></span></p></div><div><p style="text-align:center;margin-top:0;margin-bottom:0;"><a href="https://highscope.org/" target="_blank" style="font-size: 9pt;">HighScope.org</a></p></div></td></tr></tbody></table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Footer -->
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
