<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
	</head>
	<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
				<tr>
					<td align="center" valign="top">
						<div id="template_header_image">
							<table><tbody><tr><td style="padding:7.5pt 3.75pt 7.5pt 7.5pt;"><p style="text-align:center;margin-top:0;margin-bottom:0;"><a href="https://highscope.org/" target="_blank"><span style="color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo.png" alt="HighScope Logo" style="width:292.19pt;height:77.4pt;"></span></a></p></td><td style="padding:7.5pt 7.5pt 7.5pt 3.75pt;"><p style="text-align:center;margin-top:0;margin-bottom:0;"><a href="https://highscope.org/invest/" target="_blank"><span style="color: rgb(51, 51, 51); font-size: 9pt;text-decoration: none;"><img src="https://highscopedev.wpengine.com/wp-content/themes/highscope/assets/img/logo1.png" alt="HighScope 50th logo" style="width:156.6pt;height:77.4pt;"></span></a><span style="font-size: 9pt;"><u></u><u></u></span></p></td></tr></tbody></table>

						</div>
						<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
							<tr>
								<td align="center" valign="top">
									<!-- Header -->
									<table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_header">
										<tr>
											<td id="header_wrapper">
												<h1><?php echo $email_heading; ?></h1>
											</td>
										</tr>
									</table>
									<!-- End Header -->
								</td>
							</tr>
							<tr>
								<td align="center" valign="top">
									<!-- Body -->
									<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
										<tr>
											<td valign="top" id="body_content">
												<!-- Content -->
												<table border="0" cellpadding="20" cellspacing="0" width="100%">
													<tr>
														<td valign="top">
															<div id="body_content_inner">
