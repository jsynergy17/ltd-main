<?php /* Template Name: shoppage */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class('shop-page page-common'); ?>>

<div id="page-container">
    <div id="top-header">
			<div class="container clearfix">
				<div id="et-secondary-menu">
				<ul class="et-social-icons">

	<li class="et-social-icon et-social-facebook">
		<a href="https://www.facebook.com/HighScopeUS" class="icon" target="_blank">
			<span>Facebook</span>
		</a>
	</li>
	<li class="et-social-icon et-social-twitter">
		<a href="https://twitter.com/highscopeus" class="icon" target="_blank">
			<span>Twitter</span>
		</a>
	</li>

<li class="et-social-icon et-social-instagram">
<a href="https://www.instagram.com/highscopeus/" class="icon" target="_blank">
<span>Instagram</span>
</a>
</li>
<li class="et-social-icon et-social-youtube">
<a href="https://www.youtube.com/user/HighScopePreschool" class="icon" target="_blank">
<span>YouTube</span>
</a>
</li>
	<li class="et-social-icon et-social-linkedin">
<a href="https://www.linkedin.com/company/highscopeus/" class="icon" target="_blank">
<span>LinkedIn</span>
</a>
</li>
</ul><ul id="et-secondary-nav" class="menu"><li class="no-mobile menu-item menu-item-type-post_type menu-item-object-page menu-item-266579"><a href="https://highscopedev.wpengine.com/the-playground/">BLOG</a></li>
<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-65"><a href="https://highscopedev.wpengine.com/membership/" class="da11y-submenu" aria-expanded="false">HighScope Membership</a>
<ul class="sub-menu">
	<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-46710"><a href="https://highscopedev.wpengine.com/membership/" class="da11y-submenu">Join Now</a></li>
	<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="https://highscopedev.wpengine.com/log-in-2/" class="da11y-submenu">Login</a></li>
</ul>
</li>
<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-49091"><a class="da11y-submenu" aria-expanded="false">Tools</a>
<ul class="sub-menu">
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-389"><a target="_blank" rel="noopener" href="https://coradvantage.com/login/" class="da11y-submenu">COR Advantage</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-390"><a target="_blank" rel="noopener" href="https://app.redesetgrow.com/OnlineKER/" class="da11y-submenu">COR for Kindergarten</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-391"><a target="_blank" rel="noopener" href="https://app.redesetgrow.com/OnlinePQA/" class="da11y-submenu">PQA</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-392"><a target="_blank" rel="noopener" href="https://letterlinks.highscopedev.wpengine.com/login.aspx" class="da11y-submenu">Letter Links</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-393"><a target="_blank" rel="noopener" href="http://etools.highscopedev.wpengine.com/training/login/index.php" class="da11y-submenu">Professional Learning Online</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-369071"><a href="https://highscopedev.wpengine.com/contact-us/" class="da11y-submenu" aria-expanded="false">Contact Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-369078"><a href="#" class="da11y-submenu">CUSTOMER SERVICE</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-369079"><a href="mailto:media@highscope.org" class="da11y-submenu">MEDIA INQUIRES</a></li>
</ul>
</li>
<li class="button no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-255672"><a href="/annual-fund/">DONATE</a></li>
</ul>				</div> <!-- #et-secondary-menu -->

			</div> <!-- .container -->
		</div>
    <header id="main-header" data-height-onload="64" data-height-loaded="true" data-fixed-height-onload="64" style="top: 41.5px;">
			<div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="https://highscopedev.wpengine.com/">
						<img alt="HighScope" id="logo" data-height-percentage="69" data-src="https://highscopedev.wpengine.com/wp-content/uploads/2018/01/highscope-logo.png" class=" lazyloaded" src="https://highscopedev.wpengine.com/wp-content/uploads/2018/01/highscope-logo.png" data-actual-width="456" data-actual-height="67"><noscript><img src="https://highscopedev.wpengine.com/wp-content/uploads/2018/01/highscope-logo.png" alt="HighScope" id="logo" data-height-percentage="69" /></noscript>
					</a>
				</div>
				<div id="et-top-navigation">
					<nav id="top-menu-nav">
						<ul id="top-menu" class="nav">
                            <li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="da11y-submenu" aria-expanded="false">Infant-Toddler</a>
                            </li>
                            <li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="da11y-submenu" aria-expanded="false">Preschool</a>
                            </li>
                            <li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="da11y-submenu" aria-expanded="false">Early Childhood</a>
                            </li>
                            <li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="da11y-submenu" aria-expanded="false">Assessment</a>
                            </li>
                            <li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="da11y-submenu" aria-expanded="false">Professional Learning</a>
                            </li>
                            <li class="mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="da11y-submenu" aria-expanded="false">New Arrivals</a>
                            </li>
                        </ul>
                        <div id="et_top_search">
                            <span id="et_search_icon"></span>
                        </div>
                        <a href="https://highscopedev.wpengine.com/cart/" class="et-cart-icon">
                        </a>
						<div class="users_dropdown">
							<button class="dropbtn"></button>
							<div class="users_dropdown-content">
																<a href="/login/">Login</a>
								
							</div>
						</div>
					
					
					
					
					
					<div id="et_mobile_nav_menu">
				<div class="mobile_nav closed">
					<span class="select_page">Select Page</span>
					<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
				<ul id="mobile_menu" class="et_mobile_menu"><li id="menu-item-369146" class="mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-369146 et_first_mobile_item"><a href="https://highscopedev.wpengine.com/shop/" class="da11y-submenu" aria-expanded="false">Shop</a>
<ul class="sub-menu">
	<li id="menu-item-370096" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-370096"><a href="/product-category/infant-toddler/" class="da11y-submenu">Infant-Toddler Curriculum</a></li>
	<li id="menu-item-370100" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-370100"><a href="/product-category/child-assessment/" class="da11y-submenu">Child Assessment</a></li>
	<li id="menu-item-369188" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-369188"><a href="/product-category/preschool-curriculum/" class="da11y-submenu">Preschool Curriculum</a></li>
	<li id="menu-item-370101" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-370101"><a href="/product-category/program-assessment/" class="da11y-submenu">Program Assessment</a></li>
	<li id="menu-item-370102" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-370102"><a href="/product-category/professional-learning/" class="da11y-submenu">Professional Learning</a></li>
	<li id="menu-item-370103" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-370103"><a href="/product-category/uncategorized/" class="da11y-submenu">Others</a></li>
</ul>
</li>
<li id="menu-item-1814" class="mega-menu first-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1814"><a href="https://highscopedev.wpengine.com/who-we-are/" class="da11y-submenu" aria-expanded="false">Who We Are</a>
<ul class="sub-menu">
	<li id="menu-item-285509" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285509"><a href="https://highscopedev.wpengine.com/who-we-are/our-values/" class="da11y-submenu">Our Values</a></li>
	<li id="menu-item-285510" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285510"><a href="https://highscopedev.wpengine.com/who-we-are/our-history/" class="da11y-submenu">Our History</a></li>
	<li id="menu-item-285511" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285511"><a href="https://highscopedev.wpengine.com/who-we-are/our-people/" class="da11y-submenu">Our People</a></li>
	<li id="menu-item-285563" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285563"><a href="https://highscopedev.wpengine.com/who-we-are/racial-equity/" class="da11y-submenu">Our Commitment to Equity</a></li>
	<li id="menu-item-285512" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285512"><a href="https://highscopedev.wpengine.com/invest/" class="da11y-submenu">Our Anniversary</a></li>
</ul>
</li>
<li id="menu-item-285508" class="mega-menu first-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285508"><a href="https://highscopedev.wpengine.com/our-research/" class="da11y-submenu" aria-expanded="false">Our Research</a>
<ul class="sub-menu">
	<li id="menu-item-285513" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285513"><a href="https://highscopedev.wpengine.com/our-research/our-agenda/" class="da11y-submenu">Our Agenda</a></li>
	<li id="menu-item-285516" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285516"><a href="https://highscopedev.wpengine.com/current-projects/" class="da11y-submenu">Research Projects</a></li>
	<li id="menu-item-285515" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285515"><a href="https://highscopedev.wpengine.com/our-research/services-we-provide/" class="da11y-submenu">Services We Provide</a></li>
	<li id="menu-item-285517" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285517"><a href="https://highscopedev.wpengine.com/perry-preschool-project/" class="da11y-submenu">Perry Preschool Project</a></li>
</ul>
</li>
<li id="menu-item-1811" class="mega-menu first-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1811"><a href="https://highscopedev.wpengine.com/our-practice/" class="da11y-submenu" aria-expanded="false">Our Practice</a>
<ul class="sub-menu">
	<li id="menu-item-285519" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285519"><a href="https://highscopedev.wpengine.com/our-practice/our-approach/" class="da11y-submenu">Our Approach</a>
	<ul class="sub-menu">
		<li id="menu-item-369133" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-369133"><a href="https://highscopedev.wpengine.com/infosessions/" class="da11y-submenu">What, How, and Why of HighScope | Info Sessions</a></li>
		<li id="menu-item-285526" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285526"><a href="https://highscopedev.wpengine.com/active-learning-at-home/" class="da11y-submenu">Family Engagement</a></li>
	</ul>
</li>
	<li id="menu-item-285527" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285527"><a href="https://highscopedev.wpengine.com/our-practice/curriculum/" class="da11y-submenu">Curriculum</a>
	<ul class="sub-menu">
		<li id="menu-item-285528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285528"><a href="https://highscopedev.wpengine.com/our-practice/infant-toddler-curriculum/" class="da11y-submenu">Infant-Toddler Curriculum</a></li>
		<li id="menu-item-285520" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285520"><a href="https://highscopedev.wpengine.com/our-practice/preschool-curriculum/" class="da11y-submenu">Preschool Curriculum</a></li>
	</ul>
</li>
	<li id="menu-item-285529" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285529"><a href="https://highscopedev.wpengine.com/our-practice/child-assessment/" class="da11y-submenu">Assessment</a>
	<ul class="sub-menu">
		<li id="menu-item-285530" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285530"><a href="https://highscopedev.wpengine.com/cor-advantage/" class="da11y-submenu">COR Advantage</a></li>
		<li id="menu-item-285531" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285531"><a href="https://highscopedev.wpengine.com/cor-advantage-kindergarten/" class="da11y-submenu">COR for Kindergarten</a></li>
		<li id="menu-item-285532" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285532"><a href="https://highscopedev.wpengine.com/our-practice/child-assessment/pqa/" class="da11y-submenu">Program Quality Assessment (PQA)</a></li>
	</ul>
</li>
	<li id="menu-item-285521" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-285521"><a href="https://highscopedev.wpengine.com/our-practice/professional-learning/" class="da11y-submenu">Professional Learning</a>
	<ul class="sub-menu">
		<li id="menu-item-285533" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285533"><a href="https://highscopedev.wpengine.com/professional-learning-courses/" class="da11y-submenu">Course Schedule</a></li>
		<li id="menu-item-285534" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285534"><a href="https://highscopedev.wpengine.com/our-practice/certification/" class="da11y-submenu">Certification and Accreditation</a></li>
		<li id="menu-item-285535" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285535"><a href="https://highscopedev.wpengine.com/internationalconference/" class="da11y-submenu">HighScope Conference</a></li>
	</ul>
</li>
	<li id="menu-item-285539" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285539"><a href="https://highscopedev.wpengine.com/our-practice/demonstration-preschool/" class="da11y-submenu">Demonstration Preschool</a></li>
</ul>
</li>
<li id="menu-item-1812" class="mega-menu first-level menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1812 mega-menu-parent mega-menu-parent-2"><a href="https://highscopedev.wpengine.com/our-reach/" class="da11y-submenu" aria-expanded="false">Our Reach</a>
<ul class="sub-menu">
	<li id="menu-item-285540" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285540"><a href="https://highscopedev.wpengine.com/our-reach/institutes/" class="da11y-submenu">International Institutes</a></li>
	<li id="menu-item-285541" class="second-level menu-item menu-item-type-post_type menu-item-object-page menu-item-285541"><a href="https://highscopedev.wpengine.com/our-reach/united-states/" class="da11y-submenu">United States</a></li>
</ul>
</li>
<li class="menu-item wpmenucartli wpmenucart-display-standard mega-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-menu-parent mega-menu-parent-0 empty-wpmenucart" id="wpmenucartli"><a class="wpmenucart-contents empty-wpmenucart" style="display:none">&nbsp;</a></li><li class="no-mobile menu-item menu-item-type-post_type menu-item-object-page menu-item-266579"><a href="https://highscopedev.wpengine.com/the-playground/">BLOG</a></li>
<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-65"><a href="https://highscopedev.wpengine.com/membership/" class="da11y-submenu" aria-expanded="false">HighScope Membership</a>
<ul class="sub-menu">
	<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-46710"><a href="https://highscopedev.wpengine.com/membership/" class="da11y-submenu">Join Now</a></li>
	<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-67"><a href="https://highscopedev.wpengine.com/log-in-2/" class="da11y-submenu">Login</a></li>
</ul>
</li>
<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-49091"><a class="da11y-submenu" aria-expanded="false">Tools</a>
<ul class="sub-menu">
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-389"><a target="_blank" rel="noopener" href="https://coradvantage.com/login/" class="da11y-submenu">COR Advantage</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-390"><a target="_blank" rel="noopener" href="https://app.redesetgrow.com/OnlineKER/" class="da11y-submenu">COR for Kindergarten</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-391"><a target="_blank" rel="noopener" href="https://app.redesetgrow.com/OnlinePQA/" class="da11y-submenu">PQA</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-392"><a target="_blank" rel="noopener" href="https://letterlinks.highscopedev.wpengine.com/login.aspx" class="da11y-submenu">Letter Links</a></li>
	<li class="no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-393"><a target="_blank" rel="noopener" href="http://etools.highscopedev.wpengine.com/training/login/index.php" class="da11y-submenu">Professional Learning Online</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-369071"><a href="https://highscopedev.wpengine.com/contact-us/" class="da11y-submenu" aria-expanded="false">Contact Us</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-369078"><a href="#" class="da11y-submenu">CUSTOMER SERVICE</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-369079"><a href="mailto:media@highscope.org" class="da11y-submenu">MEDIA INQUIRES</a></li>
</ul>
</li>
<li class="button no-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-255672"><a href="/annual-fund/">DONATE</a></li>
</ul></div>
			</div>				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="https://highscopedev.wpengine.com/">
					<label class="da11y-screen-reader-text" for="et_pb_search_module_input_0">Search for...</label><input type="search" class="et-search-field" placeholder="Search …" value="" name="s" title="Search for:" id="et_pb_search_module_input_0"><button type="submit" class="da11y-screen-reader-text">Search</button>					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header>
        <div class="shop-banner">
        <?php
echo do_shortcode('[smartslider3 slider="2"]');
?>
        </div>
    <div id="et-main-area">
        <div id="main-content" tabindex="-1" role="main">
			<div class="container">
                <h2 class="common-head">Shop by Category</h2>
                <h6 class="common-smhead">Browse and purchase HighScope curriculum materials, assessment tools, and professional learning resources.</h6>
                <div class="row services">
                    <div class="col-2">
                        <div class="img-box">
                            <img src="/wp-content/uploads/2021/07/Lance_and_Selina_-DSC_0006.JPG.jpg">
                        </div>
                        <div class="txt-box blue-bg">
                            <h4>Professional Learning</h4>
                            <p>Our professional learning program features the best of adult learning through active engagement, collaboration, and reflection.</p>
                            <a href="#" class="btn white-transbtn">
                            SHOP NOW
                            </a>
                            
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="img-box">
                            <img src="/wp-content/uploads/2021/07/Diversity_in_classroom_Teacher_and_Child-scaled.jpg">
                        </div>
                        <div class="txt-box orange-bg">
                            <h4>Curriculum</h4>
                            <p>Our early childhood curriculum promotes children’s development and learning and provides lasting benefits into adulthood.</p>
                            <a href="#" class="btn white-transbtn">
                            SHOP NOW
                            </a>
                            
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="img-box">
                            <img src="/wp-content/uploads/2021/07/Program_Assessment_DSC0551-scaled.jpg" >
                        </div>
                        <div class="txt-box lightgreen-bg">
                            <h4>Program Assessment</h4>
                            <p>Our program quality assessment tools use proven research to measure and improve classroom and program performance.</p>
                            <a href="#" class="btn white-transbtn">
                            SHOP NOW
                            </a>
                            
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="img-box">
                            <img src="/wp-content/uploads/2021/07/Child_Assessment_MG_9923-scaled.jpg">
                        </div>
                        <div class="txt-box green-bg">
                            <h4>Child Assessment</h4>
                            <p>Our child assessment tools are proven by research and streamlined for teacher and child success.</p>
                            <a href="#" class="btn white-transbtn">
                            SHOP NOW
                            </a>
                            
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <div class="light-bg">
                <div class="container"><div class="row features">
                    <div class="col-3">
                        <div class="img-box">
<!--                            <img class="" src="/wp-content/uploads/2021/07/img-what-s-new.jpg">-->
                            <img class="" src="/wp-content/uploads/2021/07/whats-new-1.jpg">
                        </div>
                        <div class="txt-box pos-txt">
                          <h2>What’s New</h2>
<!--                          <p>It's what we've been studying for more than 50 years.</p>-->
                          <a href="btn" class="btn btn-blue">SHOP NOW</a>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="img-box">
<!--                            <img class="" src="/wp-content/uploads/2021/07/img-last-chance.jpg">-->
                            <img class="" src="/wp-content/uploads/2021/07/last-chance-1.jpg">
                        </div>
                        <div class="txt-box pos-txt">
                          <h2>Last Chance</h2>
<!--                          <p>It's what we've been studying for more than 50 years.</p>-->
                          <a href="btn" class="btn btn-blue">SHOP NOW</a>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="img-box">
<!--                            <img class="" src="/wp-content/uploads/2021/07/HighScopeCatalog2020Cover.png">-->
                            <img class="" src="/wp-content/uploads/2021/07/catalog.jpg">
                        </div>
                        <div class="txt-box pos-txt">
                          <h2>eCatalog</h2>
<!--                          <p>It's what we've been studying for more than 50 years.</p>-->
                          <a href="btn" class="btn btn-blue">SHOP NOW</a>
                        </div>
                    </div>
                </div>
             </div>
            </div>
        </div>
        <footer id="main-footer">		
            <div class="container">
                <div id="footer-widgets" class="clearfix">
                    <div class="footer-widget"><div id="custom_html-2" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Join Our Mailing List</h4><div class="textwidget custom-html-widget"><p>
                Be the first to learn about new strategies, products, and events.
            </p>
            <p>
                <a class="btn" href="https://signup.e2ma.net/signup/29012/17220/" onclick="window.open('https://signup.e2ma.net/signup/29012/17220/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up</a>
            </p>
            </div></div>
<div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Stay Connected</h4><div class="textwidget custom-html-widget"><ul class="social">
                <li><a href="https://www.facebook.com/HighScopeUS"><svg class="svg-inline--fa fa-facebook-f fa-w-9" aria-hidden="true" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512" data-fa-i2svg=""><path fill="currentColor" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path></svg><!-- <i class="fab fa-facebook-f"></i> --> <span>Facebook</span></a></li>
                <li><a href="https://twitter.com/highscopeus"><svg class="svg-inline--fa fa-twitter fa-w-16" aria-hidden="true" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg><!-- <i class="fab fa-twitter"></i> --> <span>Twitter</span></a></li>
                    <li><a href="https://www.instagram.com/highscopeus/"><svg class="svg-inline--fa fa-instagram fa-w-14" aria-hidden="true" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg><!-- <i class="fab fa-instagram"></i> --> <span>Instagram</span></a></li>
                <li><a href="https://www.linkedin.com/company/highscopeus"><svg class="svg-inline--fa fa-linkedin fa-w-14" aria-hidden="true" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z"></path></svg><!-- <i class="fab fa-linkedin"></i> --> <span>LinkedIn</span></a></li>
            </ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-6" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">SUPPORT OUR MISSION</h4><div class="textwidget custom-html-widget"><p>
                Together we can empower educators, lift families, and prepare our youngest children for school and for life.
            </p>
            <p>
                <a class="btn" href="/invest">Invest</a>
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CUSTOMER SERVICE</h4><div class="textwidget custom-html-widget"><ul class="footer-links"> 
    <li><a href="https://highscope.org/who-we-are/">About Us</a></li>
    <li><a href="#">FAQ</a></li>
    <li><a href="#">Privacy</a></li>
    <li><a href="#">Order Form</a></li>
            </ul></div></div><!-- end .fwidget --></div><div class="footer-widget"><div id="custom_html-9" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">PROFILE AND ORDERS</h4><div class="textwidget custom-html-widget"><ul class="footer-links">   
    <li><a href="#">My Account</a></li>
    <li><a href="#">My Cart</a></li>
    <li><a href="#">Order Tacking</a></li>
    <li><a href="#">My Wishlist</a></li>
    <li><a href="#">My Profile</a></li>
                    </ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-7" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CONTACT US</h4><div class="textwidget custom-html-widget"><p class="img-box with-icon location-icon">
<!--                    <img src="/wp-content/uploads/2021/07/ic-location.png">-->
                    600 North River Street<br>
                Ypsilanti, MI 48198-2898 USA</p>
                    <a href="mailto:hscst@highscope.org" class="img-box with-icon mail-icon">
<!--                        <img src="/wp-content/uploads/2021/07/ic-email.png">-->
                        hscst@highscope.org</a>
            <a href="tel:800.587.5639" class="with-icon phone-icon img-box">
<!--                <img src="/wp-content/uploads/2021/07/ic-call.png">-->
                800.587.5639
                    </a>
<!--                <a class="btn" href="mailto:hscst@highscope.org">Contact Us</a>-->
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->    </div> <!-- #footer-widgets -->
            </div>    <!-- .container -->
            <div id="footer-bottom">
					<div class="container clearfix">
										<div class="footer-logo">
                            <div class="img-box">
							<img alt="HighScope" src="/wp-content/uploads/2018/03/highscope-white.png" class="lazyload"/>
                                            </div> <span>Changing the trajectory of the world, one child at a time</span>
						</div>
						<p class="copyright">
							Copyright © 2021 HighScope Educational Research Foundation. The name "HighScope" and its corporate logos are registered trademarks and service marks of the HighScope Foundation.							
						</p>
					</div>	<!-- .container -->
				</div>
        </footer>
    </div>
</div>
<?php wp_footer(); ?>
<script type="application/javascript">
    jQuery( ".img-box img" ).each(function( index ) {  jQuery( this ).attr('src',  jQuery( this ).attr('data-src')).removeClass( "lazyload" )  });
</script>
</body>
</html>
