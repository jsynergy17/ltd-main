<?php /* Template Name: Shop Template */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();
	do_action( 'et_head_meta' );
	$template_directory_uri = get_template_directory_uri();
?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class('shop-page page-common'); ?>>

<div id="page-container">
    
 <div id="top-header">
		<div class="container clearfix">
			<div id="et-secondary-menu">
				<ul class="et-social-icons">
					<?php $social_media = get_field('social_media');
					if($social_media){ $i=0;?>
		            <?php foreach($social_media as $media){ $i++;?>
					<li class="et-social-icon et-social-<?php echo strtolower($media['name']); ?>">
						<a href="<?php echo $media['url']; ?>" class="icon" target="_blank">
							<span><?php echo $media['name']; ?></span>
						</a>
					</li>
					<?php } } ?>
				</ul>
				<ul id="et-secondary-nav" class="menu">
					<?php $top_bar_menu = get_field('top_bar_menu');
					if($top_bar_menu){ $i=0;?>
		            <?php foreach($top_bar_menu as $menu){ $i++;?>
					<li class="<?php echo ($i == count($top_bar_menu))?'button':''; ?> no-mobile menu-item menu-item-type-post_type menu-item-object-page menu-item-266579">
						<a href="<?php echo $menu['menu_url']; ?>" class="da11y-submenu" aria-expanded="false">
							<?php echo $menu['menu_name']; ?>
						</a>
					
					<?php
						if($menu['sub_menu']){ $j=0; ?>
							<ul class="sub-menu">
								<?php foreach($menu['sub_menu'] as $submenu){ $j++;?>
								<li class="no-mobile nmr-logged-out menu-item menu-item-type-post_type menu-item-object-page menu-item-46710">
									<a href="<?php echo $submenu['inner_menu_url']; ?>" class="da11y-submenu">
									<?php echo $submenu['inner_menu_name']; ?></a>
								</li>
							<?php } ?>
							</ul>
						<?php } ?>
					</li> <?php } } ?>
				</ul>
			</div> <!-- #et-secondary-menu -->
		</div> <!-- .container -->
	</div>
    <header id="main-header" data-height-onload="64" data-height-loaded="true" data-fixed-height-onload="64" style="top: 41.5px;">
			<div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="https://highscopedev.wpengine.com/">
						<img alt="HighScope" id="logo" src="<?php echo  get_field('logo'); ?>"/>
					</a>
				</div>
				<div id="et-top-navigation">
					<?php $menus = wp_get_nav_menu_items("Store Catalog" );?>
					<nav id="top-menu-nav">
						<ul id="top-menu" class="nav">
                            <?php foreach($menus as $value2){ ?>
                                <li class="<?php echo str_replace(' ', '-', $value2->title); ?> <?php echo $value2->classes[0]; ?> mega-menu menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo $value2->url; ?>" class="da11y-submenu" aria-expanded="false"><?php echo $value2->title; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        
                        <a href="/cart/" class="et-cart-icon">
                            <span class="iten-count">
                            <?php echo WC()->cart->get_cart_contents_count(); ?>
                        </span>
                        </a>
                    </nav>    
						<div class="users_dropdown">
							<button class="dropbtn"></button>
							<div class="users_dropdown-content">
								<?php if(is_user_logged_in()){ ?>
								<a href="/my-account/">My Account</a>
								<?php }else{?>
								<a href="/my-account/">Sign In</a>
								<?php } ?>	
							</div>
						</div>
                     
						<?php if ( $et_slide_header || is_customize_preview() ) : ?>
						<span class="mobile_menu_bar et_pb_header_toggle et_toggle_<?php echo esc_attr( et_get_option( 'header_style', 'left' ) ); ?>_menu"></span>
					<?php endif; ?>
                    
                    <?php

					/**
					 * Fires at the end of the 'et-top-navigation' element, just before its closing tag.
					 *
					 * @since 1.0
					 */
					do_action( 'et_header_top' );

					?>
					

				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
            <div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Divi' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Divi' )
						);

						/**
						 * Fires inside the search form element, just before its closing tag.
						 *
						 * @since ??
						 */
						do_action( 'et_search_form_fields' );
					?>
					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
<!--
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<?php //echo do_shortcode('[smart_search id="1"]'); ?>
					<span class="et_close_search_field"></span>
				</div>
			</div>
-->
		</header>
    <div class="shop-banner">
    	<?php echo do_shortcode('[smartslider3 slider="2"]'); ?>
    </div>
    <div id="et-main-area">
        <div id="main-content" tabindex="-1" role="main">
			<div class="container">
                <h2 class="common-head"><?php echo  get_field('shop_by_category_title'); ?></h2>
                <h6 class="common-smhead"><?php echo  get_field('shop_by_category_descriptions'); ?></h6>
                <div class="row services">
                    <?php $category_details = get_field('category_details');
                    if($category_details){ $i=0;?>
                    <?php foreach($category_details as $details){ $i++;?>
                    <div class="col-2">
                        <div class="img-box">
                            <img src="<?php echo $details['image']; ?>">
                        </div>
                        <div class="txt-box <?php echo $details['class_name']; ?>">
                            <h4><?php echo $details['category_name']; ?></h4>
                            <p><?php echo $details['category_description']; ?></p>
                            <a href="<?php echo $details['shop_url']; ?>" class="btn white-transbtn">
                            <?php echo $details['shop_now']; ?>
                            </a>
                            
                        </div>
                    </div>
                    <?php } } ?>
                </div>
            </div>
            <div class="light-bg">
                <div class="container"><div class="row features">
                    <?php $features = get_field('features');
                    if($features){ $i=0;?>
                    <?php foreach($features as $data){ $i++;?>
                    <div class="col-3">
                        <div class="img-box">
                            <img class="" src="<?php echo $data['image']; ?>">
                        </div>
                        <div class="txt-box pos-txt">
                          <h2><?php echo $data['title']; ?></h2>
                            <a href="<?php echo $data['shop_now_url']; ?>" class="btn btn-blue"><?php echo $data['shop_now']; ?></a>
                        </div>
                    </div>
                    <?php } } ?>
                </div>
             </div>
            </div>
        </div>
        <footer id="main-footer">		
            <div class="container">
                <div id="footer-widgets" class="clearfix">
                    <div class="footer-widget">
                        <div id="custom_html-2" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">Join Our Mailing List</h4>
                            <div class="textwidget custom-html-widget">
                                <p>Be the first to learn about new strategies, products, and events.</p>
                                <p>
                                <a class="btn" href="https://signup.e2ma.net/signup/29012/17220/" onclick="window.open('https://signup.e2ma.net/signup/29012/17220/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up</a>
                                </p>
                            </div>
                        </div>
                        <div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html">
                            <h4 class="title">Stay Connected</h4>
                            <div class="textwidget custom-html-widget">
                                <ul class="social">
                                    <li><a href="https://www.facebook.com/HighScopeUS"> 
                                        <img src="/wp-content/themes/highscope/assets/img/facebook.svg"><span>Facebook</span></a></li>
                                    <li><a href="https://twitter.com/highscopeus"><img src="/wp-content/themes/highscope/assets/img/twitter.svg"><span>Twitter</span></a></li>
                                    <li><a href="https://www.instagram.com/highscopeus/"><img src="/wp-content/themes/highscope/assets/img/instagram.svg"><span>Instagram</span></a></li>
                                    <li><a href="https://www.linkedin.com/company/highscopeus"><img src="/wp-content/themes/highscope/assets/img/linkedin.svg"> <span>LinkedIn</span></a></li>
            </ul></div>
        </div> <!-- end .fwidget --></div> <!-- end .footer-widget -->
        <div class="footer-widget">
            <div id="custom_html-6" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">SUPPORT OUR MISSION</h4><div class="textwidget custom-html-widget"><p>
                Together we can empower educators, lift families, and prepare our youngest children for school and for life.
            </p>
            <p>
                <a class="btn" href="/invest">Invest</a>
            </p></div>
        </div> <!-- end .fwidget -->
        </div> <!-- end .footer-widget -->
        <div class="footer-widget"><div id="custom_html-3" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CUSTOMER SERVICE</h4><div class="textwidget custom-html-widget"><ul class="footer-links"> 
    <li><a href="https://highscope.org/who-we-are/">About Us</a></li>
    <li><a href="/faq/">FAQ</a></li>
    <li><a href="/policies/">Terms & Policies</a></li>
            </ul></div></div><!-- end .fwidget --></div><div class="footer-widget"><div id="custom_html-9" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">PROFILE AND ORDERS</h4>
            <div class="textwidget custom-html-widget">
                <ul class="footer-links">
                <?php if(is_user_logged_in()){ ?>      
                        <li><a href="/my-account/">My Account</a></li>
                        <li><a href="/cart/">My Cart</a></li>
                        <li><a href="#">Order Tracking</a></li>
                        <li><a href="#">My Wishlist</a></li>
                        <li><a href="/my-account/edit-account/">My Profile</a></li>
                <?php }else{?>  
                        <li><a href="/my-account/">My Account</a></li>
                <?php } ?>          
                    </ul>
             </div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="custom_html-7" class="widget_text fwidget et_pb_widget widget_custom_html"><h4 class="title">CONTACT US</h4><div class="textwidget custom-html-widget"><p class="img-box with-icon location-icon">
<!--                    <img src="/wp-content/uploads/2021/07/ic-location.png">-->
                    600 North River Street<br>
                Ypsilanti, MI 48198-2898 USA</p>
                    <a href="mailto:hscst@highscope.org" class="img-box with-icon mail-icon">
<!--                        <img src="/wp-content/uploads/2021/07/ic-email.png">-->
                        hscst@highscope.org</a>
            <a href="tel:800.587.5639" class="with-icon phone-icon img-box">
<!--                <img src="/wp-content/uploads/2021/07/ic-call.png">-->
                800.587.5639
                    </a>
<!--                <a class="btn" href="mailto:hscst@highscope.org">Contact Us</a>-->
            </p></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->    </div> <!-- #footer-widgets -->
            </div>    <!-- .container -->
            <div id="footer-bottom">
					<div class="container clearfix">
										<div class="footer-logo">
                            <div class="img-box">
							<img alt="HighScope" src="/wp-content/uploads/2018/03/highscope-white.png" class="lazyload"/>
                                            </div> <span>Changing the trajectory of the world, one child at a time</span>
						</div>
						<p class="copyright">
							Copyright © 2021 HighScope Educational Research Foundation. The name "HighScope" and its corporate logos are registered trademarks and service marks of the HighScope Foundation.							
						</p>
					</div>	<!-- .container -->
				</div>
        </footer>
    </div>
</div>
<?php wp_footer(); ?>
<script type="application/javascript">
    jQuery( ".img-box img" ).each(function( index ) {  jQuery( this ).attr('src',  jQuery( this ).attr('data-src')).removeClass( "lazyload" )  });
</script>
</body>
</html>
