<html><head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
	<meta content="width=device-width" name="viewport">
	<title></title>
	
	
	
	
</head>

<body style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #fff;">
	<table border="0" cellspacing="0" cellpadding="0" style="width:480pt; padding: 11.25pt 0" align="center">
<tbody><tr>
<td valign="top" style="padding:0;">
    <table border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
    <td valign="top" style="padding:7.5pt 3.75pt 7.5pt 7.5pt;">
    <p align="center" style="text-align:center;margin-top:0;margin-bottom:0;"><a href="https://highscope.org/" target="_blank"><span style="color: rgb(51, 51, 51); font-size: 9pt; font-family: Arial, sans-serif, serif, EmojiFont; text-decoration: none;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/8124a5cb070288947c89e0e7_780x206.png" border="0" alt="HighScope Logo" style="width:292.19pt;height:77.4pt;"></span></a></p>
    </td>
    <td valign="top" style="padding:7.5pt 7.5pt 7.5pt 3.75pt;">
    <p align="center" style="text-align:center;margin-top:0;margin-bottom:0;"><a href="https://highscope.org/invest/" target="_blank"><span style="color: rgb(51, 51, 51); font-size: 9pt; font-family: Arial, sans-serif, serif, EmojiFont; text-decoration: none;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/d249b0b8a9ea5acd9668a662_418x206.png" alt="HighScope 50th logo" style="width:156.6pt;height:77.4pt;"></span></a><span style="font-size: 9pt; font-family: Arial, sans-serif, serif, EmojiFont;"><u></u><u></u></span></p>
    </td>
</tr>
</tbody></table>
</td>
</tr><tr><td><div style="border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;"></div></td></tr><tr><td>
    <p style="margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;font-family: &quot;Century Gothic&quot;, sans-serif;">Dear HighScope Customer,</p><p style="margin-top:0;margin-bottom: 7.5px;color: rgb(40, 40, 40);font-size: 12pt;font-family: &quot;Century Gothic&quot;, sans-serif;">Thank you for purchasing HighScope products to use in your classroom and demonstrating your commitment to creating high-quality
learning experiences for early learners! Whether your program uses the full HighScope Curriculum, program or child assessment, or just a few supplementary materials, your purchase helped ensure that HighScope can continue to innovate and advocate for a future
where every child, regardless of circumstance, can access a high-quality early childhood education.</p><p style="margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;font-family: &quot;Century Gothic&quot;, sans-serif;">This is a courtesy notification that HighScope will be making some changes to our online store beginning on August 1. Our updated
site will have improved keyword navigation, contemporary icons and images, and a user interface that more closely matches our current brand. You will still be able to find the HighScope Online Store at this location(<a href="https://secure.highscope.org/" target="_blank" style="color: rgb(51, 51, 51);font-size: 12pt;font-family: &quot;Century Gothic&quot;, sans-serif;">secure.highscope.org</a>).</p>
<p style="margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;font-family: &quot;Century Gothic&quot;, sans-serif;">Following the updated store launch on August 1, you will receive an email prompting you to reset your HighScope Store password.
To streamline your next shopping experience with HighScope, please follow the prompt and reset your login information.</p>
<p style="margin-top:0;margin-bottom: 7.5pt;color: rgb(40, 40, 40);font-size: 12pt;font-family: &quot;Century Gothic&quot;, sans-serif;">We hope these changes will improve the experience of ordering HighScope products so you can focus on what matters most: the young
learners in your classroom. Thank you for supporting HighScope, a nonprofit early childhood organization. We hope you enjoy the new look of the HighScope Online Store!</p>
</td></tr><tr>
    <td style="
    text-align: center;
">
<a href="https://secure.highscope.org/" target="_blank" style="
    color: white;
    font-size: 13.5pt;
    font-family: &quot;Century Gothic&quot;, sans-serif, serif, EmojiFont;
    background-color: rgb(1, 33, 105);
    text-decoration: none;
    padding: 12pt;
    border: 1pt solid rgb(1, 33, 105);
    font-weight: bold;
    display: inline-block;
    margin: 5pt auto 0pt;
    border-radius: 6px;
">SEE
WHAT'S NEW</a>
</td>

</tr>
<tr><td><div style="border-top: 2px solid #ac87878f;padding-bottom: 20pt;margin-top: 20pt;"></div></td></tr><tr>
<td style="
    text-align: center;
">
    <ul style="
    list-style: none;
    display: flex;
    justify-content: center;
    align-items: center;
">
        <li>
            <a href="https://twitter.com/HighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;font-family: Arial, sans-serif, serif, EmojiFont;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/color-circle-twitter.png" border="0" alt="Twitter" style="width:18pt;height:18pt;"></a>
        </li>
         <li>
            <a href="https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2FHighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;font-family: Arial, sans-serif, serif, EmojiFont;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/color-circle-facebook.png" border="0" alt="Twitter" style="width:18pt;height:18pt;"></a>
        </li>
         <li>
            <a href="https://www.instagram.com/HighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;font-family: Arial, sans-serif, serif, EmojiFont;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/color-circle-instagram.png" border="0" alt="Twitter" style="width:18pt;height:18pt;"></a>
        </li>
         <li>
            <a href="https://www.linkedin.com/company/highscopeus" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;font-family: Arial, sans-serif, serif, EmojiFont;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/color-circle-linkedin.png" border="0" alt="Twitter" style="width:18pt;height:18pt;"></a>
        </li>
         <li>
            <a href="https://twitter.com/HighScopeUS" target="_blank" style="color: rgb(51, 51, 51);font-size: 9pt;font-family: Arial, sans-serif, serif, EmojiFont;text-decoration: none;padding: 3.2px;"><img src="https://highscopedev.wpengine.com/wp-content/uploads/2021/07/color-circle-twitter.png" border="0" alt="Twitter" style="width:18pt;height:18pt;"></a>
        </li>
    </ul>
</td>
</tr>

<tr>
<td style="background-color:#CCCCCC; padding:7.5pt;">
<div>
<p align="center" style="text-align:center;margin-top:0;margin-bottom:0;"><span style="font-size: 9pt; font-family: Arial, sans-serif, serif, EmojiFont;">HighScope Educational Research Foundation<u></u><u></u></span></p>
</div>
<div>
<p style="text-align:center;margin-top:0;margin-bottom:0;"><a href="https://highscope.org/" target="_blank" style="font-size: 9pt; font-family: Arial, sans-serif, serif, EmojiFont;">HighScope.org
</a></p>
</div>
</td>
</tr>
 </tbody></table>
    
</body></html>