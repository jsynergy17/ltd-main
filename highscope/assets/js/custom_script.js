/******************* Custom Jquery File **********************/
var PClevel = '';
jQuery(document).ready(function() {
    jQuery('.wpmenucart-contents .cartcontents').each(function(){
        jQuery(this).html(jQuery(this).html().split(" item").join(""));
        jQuery(this).html(jQuery(this).html().split("s").join(""));
    });
    jQuery(".product-page .product-categories > li").click(function(e) {
        if(!(e.target.nodeName == "A") && !(jQuery(e.target).parent('ul').hasClass("children")) && !(jQuery(e.target).hasClass("children"))){
            if(jQuery(this).hasClass("active")){
               jQuery(this).removeClass("active"); 
            }else{
               jQuery(".product-page .product-categories > li").removeClass("active");
               jQuery(this).addClass("active"); 
            }  
        }
    });
    jQuery(".page-common Header form[role='search']").addClass("et-search-form"); 
    jQuery(".page-common Header form[role='search'] input").addClass("et-search-field").removeClass("search-field");
    
    
	jQuery(".per_page_data").change(function() {
		var loc = location.href;
		if (loc.indexOf("?") === -1)
		loc += "?";
		else
		loc += "&";
		location.href = loc + "per_page="+jQuery(this).val();
	});
    jQuery(".archive .addSpace").text(">");
    text = jQuery(".archive .woocommerce-result-count").text().replace("Showing all ", "");
    text2 =  text.replace("results", "Result(s)");
    jQuery(".archive .woocommerce-result-count").text(text2);
    
    jQuery( ".archive .products" ).before( "<div class='ListingHeader'></div>" );
    jQuery( ".archive .ListingHeader" ).append( jQuery( ".woocommerce-result-count" ),jQuery( ".gridlist-toggle" ),jQuery(".result_per_page"),jQuery(".woocommerce-ordering"),jQuery(".woocommerce-ordering"));
    jQuery( ".archive .woocommerce-ordering" ).prepend("<span class='text'> Sort by</span>");
    
    


    jQuery(document).on('click', '.plus-add',function() {
        jQuery('.let-err').remove();
        jQuery('.eror-border').removeClass('eror-border');
        var _fname = jQuery(this).parent('div.inner_multiples').find('input[name="fname[]"]');
        var _lname = jQuery(this).parent('div.inner_multiples').find('input[name="lname[]"]');
    	var _ename = jQuery(this).parent('div.inner_multiples').find('input[name="ename[]"]');

        var _flag = 0;
        if(_ename.val() == ''){
        	_ename.addClass('eror-border');
            jQuery(this).parent('div.inner_multiples').after('<p class="let-err _ename-err">please enter email.</p>');
            _flag = 1;
        }
        if(_ename.val() != '' && !isEmail(_ename.val())){
        	_ename.addClass('eror-border');
            jQuery(this).parent('div.inner_multiples').after('<p class="let-err _ename-err">please enter valid email.</p>');
            _flag = 1;
        }
        if(_lname.val() == ''){
        	_lname.addClass('eror-border');
            jQuery(this).parent('div.inner_multiples').after('<p class="let-err _lname-err">please enter last name.</p>');
            _flag = 1;
        }
        
        if(_fname.val() == ''){
        	_fname.addClass('eror-border');
            jQuery(this).parent('div.inner_multiples').after('<p class="let-err _fname-err">please enter first name.</p>');
            _flag = 1;
        }
        if(_flag == 0){
           jQuery('.let-err').remove(); 
           var _html = jQuery(this).parent('div.inner_multiples').html();
    	   jQuery('div.multiple').find('div.inner_multiples').addClass('initial');
    	   jQuery('div.multiple').append('<div class="inner_multiples">'+_html+'</div>');
	    }
    });

	jQuery(document).on('click', '.remove-minus',function() {
    	jQuery(this).parent('div.inner_multiples').remove();
    	if(jQuery('div.multiple').find('div.inner_multiples').length > 1){
    		jQuery('div.multiple').find('div.inner_multiples:last').removeClass('initial');
    	}
	});

	jQuery(document).on('keyup', '.eror-border',function() {
    	var _rmv_error = jQuery(this).attr('err_name');
    	jQuery('.'+_rmv_error).remove();
        jQuery(this).removeClass('eror-border');
	});

    jQuery(document).on('change', '#billing_country',function() {
    	var _other_add = jQuery('#ship-to-different-address-checkbox').prop('checked');
        if(!_other_add){
	        var _country = jQuery(this).val();
	        var _lable = jQuery("option:selected", this).text();
	        if(_country != 'US'){
	        	PClevel = _lable;
				swal({
                    text: "Currently, an international shipping option is not available. Do you still want to order for the United Kingdom (UK) location? Please click Continue to send an email to High Scope Customer Support or contact us at the details below.\npress@highscope.org | 734-485-2000 ext 279 or 273",
	                icon: "warning",
	                buttons: ["Cancel", "Continue"],
	                dangerMode: true,
	                className: "sender-mail",
	                })
	                .then((willDelete) => {
	                if (willDelete) {
	                    swal("Thank you for contacting the High Scope team! We have received your request and will contact you within 48 hours to process this order for the "+_lable+" location.", {
	                        icon: "success",
	                    });
	                    jQuery('#billing_country').val('US').trigger('change');
	                }else{
	                    jQuery('#billing_country').val('US').trigger('change');
	                }
	            });
	        }
	    }    
    });

    jQuery(document).on('change', '#shipping_country',function() {
        var _country = jQuery(this).val();
        var _lable = jQuery("option:selected", this).text();
        if(_country != 'US'){
        	PClevel = _lable;
			swal({
                text: "Currently, an international shipping option is not available. Do you still want to order for the United Kingdom (UK) location? Please click Continue to send an email to High Scope Customer Support or contact us at the details below.\npress@highscope.org | 734-485-2000 ext 279 or 273",
                icon: "warning",
                buttons: ["Cancel", "Continue"],
                dangerMode: true,
                className: "sender-mail",
                })
                .then((willDelete) => {
                if (willDelete) {
                    swal("Thank you for contacting the High Scope team! We have received your request and will contact you within 48 hours to process this order for the "+_lable+" location.", {
                        icon: "success",
                    });
                    jQuery('#shipping_country').val('US').trigger('change');
                }else{
                    jQuery('#shipping_country').val('US').trigger('change');
                }
            });
        }
    });

    jQuery('#ship-to-different-address-checkbox').change(function(){
    	var _other_add = jQuery(this).prop('checked');
    	if(!_other_add){
    	  jQuery('#billing_country').val('US').trigger('change');
    	}
	});


    jQuery(document).on('click', 'button.swal-button.swal-button--confirm.swal-button--danger',function(){
    	var _fname = jQuery('#billing_first_name').val();
    	var _lname = jQuery('#billing_last_name').val();
    	var _mail = jQuery('#billing_email').val();
    	var _billing_address_1 = jQuery('#billing_address_1').val();
    	var _billing_city = jQuery('#billing_city').val();
    	var _billing_postcode = jQuery('#billing_postcode').val();
    	var _billing_phone = jQuery('#billing_phone').val();

    	var data = {
			'action': 'send_mail_internation_ship',
			'fname': _fname,
			'lname': _lname,
			'mail': _mail,
			'billing_address_1' : _billing_address_1,
			'billing_city' : _billing_city,
			'billing_postcode' : _billing_postcode,
			'billing_phone' : _billing_phone,
			'location' : PClevel,
		};
		jQuery.ajax({
			url: ajax_url,
			type: 'post',
			data: data,
			dataType: 'json',
			success: function(data){
				if(data.status == 1){
					console.log('success');
				}
			}
		});
	});	

    jQuery(document).on('keyup', '#virtual_box',function() {
    	var txtVal = jQuery(this).val();
        jQuery('#order_comments').val(txtVal);
    });

    jQuery('.tax_exempts').click(function() {
        jQuery('.let-err').remove();
        jQuery('select[name="exempt_type"]').val('');
        jQuery('select[name="exempt_states[]"]').val('');
        jQuery('input[name="certificate_expire"]').val('');
        jQuery('input[name="pdf_base649"]').val('');

        if(jQuery(this).is(":checked")) {
            jQuery('div.tax-expept-process').removeClass('hide');
        }else{
            jQuery('div.tax-expept-process').addClass('hide');
        }
    });

    jQuery(document).on('change','#files' , function(e){ 
        handleFileSelect(e);
    });

    jQuery('.save_tax_jars').click(function() {
        jQuery('.let-err').remove();
        var _exempt_type = jQuery('select[name="exempt_type"]');
        var _exempt_states = jQuery('select[name="exempt_states[]"]');
        var _certificate_expire = jQuery('input[name="certificate_expire"]');
        var _pdf_base649 = jQuery('input[name="pdf_base649"]');
        var _flag = 0;
        if(_exempt_type.val() == ''){
            _exempt_type.after('<p class="let-err _fname-err">please select exempt type.</p>');
            _flag = 1;
        }
        if(_exempt_states.val() == ''){
            _exempt_states.after('<p class="let-err _fname-err">please select exempt states.</p>');
            _flag = 1;
        }
        if(_pdf_base649.val() == ''){
            _pdf_base649.after('<p class="let-err _fname-err">please select tax certificate.</p>');
            _flag = 1;
        }
        if(_certificate_expire.val() == ''){
            _certificate_expire.after('<p class="let-err _fname-err">please enter certificate expire date.</p>');
            _flag = 1;
        }

        if(_flag == 0){
            var data = {
                'action': 'send_tax_exempt_ajax',
                'exempt_type': _exempt_type.val(),
                'exempt_states': _exempt_states.val(),
                'pdf_base': _pdf_base649.val(),
                'certificate_expire' : _certificate_expire.val(),
            };

            jQuery.ajax({
                url: ajax_url,
                type: 'post',
                data: data,
                dataType: 'json',
                success: function(data){
                    if(data.status == 1){
                        swal("Thanks!", "We will process your request!", "success");
                        jQuery('.tax_exempts').prop('checked', false);
                        jQuery('div.tax-expept-process').addClass('hide');
                        jQuery('.tax_exempts').attr('disabled', true);
                    }
                }
            });
        }
    });


    jQuery('select[name="orderby"]').change(function(e){ 
        var _sorter = jQuery(this).val();
        var _url = window.location.href;
        window.location.href = _url+'&order='+_sorter;
        
    });
        
    jQuery('.woocommerce-form-registrations').submit(function(e){ 
        jQuery('.let-err').remove();
        var _title = jQuery('select[name="title"]');
        var _first_name = jQuery('input[name="first_name"]');
        var _last_name = jQuery('input[name="last_name"]');
        var _emails = jQuery('input[name="emails"]');
        var _confirm_email = jQuery('input[name="confirm_email"]');
        var _passwords = jQuery('input[name="passwords"]');
        var _cpassword = jQuery('input[name="cpassword"]');
        var _address = jQuery('input[name="address"]');
        var _city = jQuery('input[name="city"]');
        var _zipcode = jQuery('input[name="zipcode"]');
        var _phone = jQuery('input[name="phone"]');
        var _exempt_type = jQuery('select[name="exempt_type"]');
        var _exempt_states = jQuery('select[name="exempt_states[]"]');
        var _certificate_expire = jQuery('input[name="certificate_expire"]');
        var _pdf_base649 = jQuery('input[name="pdf_base649"]');
        var _flag = 0;

        if(_exempt_type.val() != ''){
            if(_exempt_states.val() == ''){
                _exempt_states.after('<p class="let-err _fname-err">Please select exempt states.</p>');
                _flag = 1;
            }
            if(_pdf_base649.val() == ''){
                _pdf_base649.after('<p class="let-err _fname-err">Please select tax certificate.</p>');
                _flag = 1;
            }
            if(_certificate_expire.val() == ''){
                _certificate_expire.after('<p class="let-err _fname-err">Please enter certificate expire date.</p>');
                _flag = 1;
            }
        } 
        if(_phone.val() == ''){
            _phone.after('<p class="let-err _fname-err">Please enter phone.</p>');
            _flag = 1;
            _phone.focus();
        } 
        if(_zipcode.val() == ''){
            _zipcode.after('<p class="let-err _fname-err">Please enter zipcode.</p>');
            _flag = 1;
            _zipcode.focus();
        }
        if(_city.val() == ''){
            _city.after('<p class="let-err _fname-err">Please enter city.</p>');
            _flag = 1;
            _city.focus();
        }
        if(_address.val() == ''){
            _address.after('<p class="let-err _fname-err">Please enter address.</p>');
            _flag = 1;
            _address.focus();
        }
        if((_passwords.val() != _cpassword.val()) && _cpassword.val() != ''){
            _cpassword.after('<p class="let-err _fname-err">Password does not match.</p>');
            _flag = 1;
            _cpassword.focus();
        }
        if(_cpassword.val() == ''){
            _cpassword.after('<p class="let-err _fname-err">Please enter confirm password.</p>');
            _flag = 1;
            _cpassword.focus();
        }
        if(_passwords.val() == ''){
            _passwords.after('<p class="let-err _fname-err">Please enter password.</p>');
            _flag = 1;
            _passwords.focus();
        }
        if((_emails.val() != _confirm_email.val()) && _confirm_email.val() != ''){
            _confirm_email.after('<p class="let-err _fname-err">Email does not match.</p>');
            _flag = 1;
            _confirm_email.focus();
        }
        if(_confirm_email.val() == ''){
            _confirm_email.after('<p class="let-err _fname-err">Please enter confirm email.</p>');
            _flag = 1;
            _confirm_email.focus();
        }
        if(_emails.val() == ''){
            _emails.after('<p class="let-err _fname-err">Please enter email.</p>');
            _flag = 1;
            _emails.focus();
        }
        if(_last_name.val() == ''){
            _last_name.after('<p class="let-err _fname-err">Please enter last name.</p>');
            _flag = 1;
            _last_name.focus();
        }
        if(_first_name.val() == ''){
            _first_name.after('<p class="let-err _fname-err">Please enter first name.</p>');
            _flag = 1;
            _first_name.focus();
        }  
        if(_flag == 1){
            return false;
        }
    });
    
});


function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}


function handleFileSelect(evt) {
    var f = evt.target.files[0]; // FileList object
    if(f.type == 'application/pdf'){
        var reader = new FileReader();
        reader.onload = (function(theFile) {
        return function(e) {
            var binaryData = e.target.result;
            var base64String = window.btoa(binaryData);
            document.getElementById('pdf_base649').value = base64String;
            jQuery('#pdf_base649').val(base64String);
        };
        })(f);
        reader.readAsBinaryString(f);
    }else{
        alert('Please select pdf file');
        document.getElementById('files').value=''
    }   
}

jQuery(function(){
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var maxDate = year + '-' + month + '-' + day;
    jQuery('input[name="certificate_expire"]').attr('min', maxDate);
});	